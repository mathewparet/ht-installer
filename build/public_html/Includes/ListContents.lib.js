			var AjaxListContents=new Ajax("index.php","GET");
			var ListContents_sid=CreateStatus();			
			AjaxListContents.onResponse=function(r)
			{
				if(r.State==4)
				{
					SetStatus('',ListContents_sid);
					var xmldoc=r.XML;
					var root=xmldoc.getElementsByTagName("subfile");
					var mval='';
					document.getElementById("SubFileList").innerHTML='';
					for(var i=0;i<root.length;i++)
					{
						AddSubFile(root[i].getAttribute('name'),root[i].getAttribute('type'));
					}
					document.getElementById("SubPath").value=mval;
					SetFocusXML(xmldoc);
					ShowMessages(xmldoc);
				}
			}
			
			AjaxListContents.onError=function(e)
			{
				alert("Communication error in List Contents: "+e);
				SetStatus('',ListContents_sid);
			}
			
			function ListContents(f)
			{
				pack=document.getElementById("InstallPackageList").value;
				if(pack=="===| Select Package |===")
				{
					alert("Please choose a package first!");
					return;
				}
				document.getElementById("SubFileListBox").style.display="block";				
				SetStatus("Listing contents of package ("+pack+")",ListContents_sid);
				if(f!='')
					AjaxListContents.Go("Action=ListContents&Package="+pack+"&Home="+f);				
				else
					AjaxListContents.Go("Action=ListContents&Package="+pack);
			}
			
			function AddSubFile(FName,Type)
			{
				fl=document.getElementById("SubFileList").innerHTML;
				if(Type=='folder')
					fl=fl+"<tr><td>&nbsp;</td><td><a href='#' onClick='javascript: ListContents(\""+FName+"\"); return false;'>"+FName+"</a> - <a href='#' onClick='javascript: SetSubFile(\""+FName+"\"); return false;'>[Choose]</a></td></tr>";
				else if(Type=="home")
					fl=fl+"<tr><td>&nbsp;</td><td><a href='#' onClick='javascript: ListContents(\""+FName+"\"); return false;'>Up One Level</a> <font color=black> - ("+FName+")</font></td></tr><tr><td>&nbsp;</td></tr>";
				else
					fl=fl+"<tr><td>&nbsp;</td><td>"+FName+"</td></tr><tr><td>&nbsp;</td></tr>";
					
				document.getElementById("SubFileList").innerHTML=fl;
			}
			
			function SetSubFile(FName)
			{
				document.getElementById("SubPath").value=FName;
				document.getElementById("SubFileListBox").style.display="none";
				document.getElementById("SubFileList").innerHTML='';
			}
