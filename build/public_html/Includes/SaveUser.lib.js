var SaveUserAjax = new Ajax("index.php","GET");
var SaveUser_sid=CreateStatus();

function SaveUser()
{
	vUserName=document.getElementById("UserName").value;
	vOldPassword=document.getElementById("OldPassword").value;
	vNewPassword=document.getElementById("NewPassword").value;
	vRePassword=document.getElementById("RePassword").value;
	SetStatus("Updating user information",SaveUser_sid);
	SaveUserAjax.Go("Action=SaveUser&UserName="+vUserName+"&OldPassword="+vOldPassword+"&NewPassword="+vNewPassword+"&RePassword="+vRePassword);
}

SaveUserAjax.onResponse=function(r)
{
	if(r.State==4)
	{
		SetStatus('',SaveUser_sid);
		var xmldoc=r.XML;
		ShowMessages(xmldoc);
		document.getElementById("OldPassword").value='';
		document.getElementById("NewPassword").value='';
		document.getElementById("RePassword").value='';
	}
}