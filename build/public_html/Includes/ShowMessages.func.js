			function ShowMessages(xmldoc)
			{
					root=xmldoc.getElementsByTagName("message");
					if(root.length)
						document.getElementById("msg").innerHTML="";
					else
						return;
					var arMsgInf=new Array();
					var arMsgErr=new Array();
					for(i=0;i<root.length;i++)
					{
						if(root[i].getAttribute("type")=="info")
							arMsgInf.push(root[i].childNodes[0].nodeValue);
						else
							arMsgErr.push(root[i].childNodes[0].nodeValue);
					}
					if(arMsgInf.length)
					{
						var mData="<div id='info'><ul>";
						for(i=0;i<arMsgInf.length;i++)
							mData=mData+"<li>"+arMsgInf[i]+"</li>";
						mData=mData+"</ul></div>";
						document.getElementById("msg").innerHTML=mData;
					}
					if(arMsgErr.length)
					{
						var mData=document.getElementById("msg").innerHTML;
						mData=mData+"<div id='failure'><ul>";
						for(i=0;i<arMsgErr.length;i++)
							mData=mData+"<li>"+arMsgErr[i]+"</li>";
						mData=mData+"</ul></div>";
						document.getElementById("msg").innerHTML=mData;
					}
			}
