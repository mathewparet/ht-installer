function PerfectSize(val)
{
	if(val<1024)
		return val+" bytes";
	if(val>=1024 && val<(1024*1024))
		return Math.floor(val/1024)+" Kb";
	else
		return Math.floor(val/(1024*1024))+" Mb";
}
