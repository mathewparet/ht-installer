var AjaxLogView=new Ajax("index.php","GET");
var LogView_sid=CreateStatus();
var Start=0;
var PageSize=10;

function LoadLog(Start)
{
	SetStatus("Parsing log entries",LogView_sid);
	AjaxLogView.Go("Action=ParseLogEntries&Start="+Start);
}

AjaxLogView.onResponse=function(r)
{
	if(r.State==4)
	{
		xmldoc=r.XML;
		flag=false;
		SetStatus('',LogView_sid)
		var root=xmldoc.getElementsByTagName("log");
		document.getElementById("LogDisplayID").innerHTML='';
		for(var i=0;i<root.length;i++)
		{
			AddLogDisplay(root[i].getAttribute('date'),root[i].childNodes[0].nodeValue,root[i].getAttribute('username'),root[i].getAttribute('type'),root[i].getAttribute('ip'),root[i].getAttribute('referer'),unescape(root[i].getAttribute('query')));
		}
		var root=xmldoc.getElementsByTagName("var");
		for(i=0;i<root.length;i++)
		{
			if(root[i].getAttribute("name")=="Start")
				Start=root[i].getAttribute("value");
			if(root[i].getAttribute("name")=="PageSize")
				PageSize=root[i].getAttribute("value");
			if(root[i].getAttribute("name")=="FileSize")
			{
				document.getElementById("LogFileSize").innerHTML=root[i].getAttribute("value");
				flag=true;
			}
		}
		if((Start-(PageSize*2))>0)
			Next=Start-(PageSize*2);
		else
			Next=0;
		if(!flag)
			document.getElementById("DescData").innerHTML="Log entries. <a href='#' onClick='javascript: LoadLog("+Next+"); return false;'>&lt; Newer</a> - <a href='#' onClick='javascript: LoadLog("+Start+"); return false;'>Older &gt;</a> - <a href='#' onClick='javascript: ClearLog(); return false;'>Clear Log</a>";
	}
}

function ClearLog()
{
	if(confirm("Are you sure you wish to clear the log entries from the server? This process cannot be undone!!\n\nOK => Clear\nCancel => Do Not Clear"))
	{
		SetStatus('Clearing log entries');
		AjaxLogView.Go("Action=ClearLogEntries")
	}
}

function AddLogDisplay(date,message,username,type,ip,referer,query)
{
	var tblLog=document.getElementById("LogDisplayID");
	var data="	<tr id='"+type+"'>";
	data=data+"		<td width='147'>"+date+"</td>";
	data=data+"		<td width='52'>"+type+"</td>";
	data=data+"		<td width='100%'>"+message+"</td>";
	data=data+"		<td width='113'><a href='#' onClick='javascript: ShowDetails(\""+date+"\",\""+type+"\",\""+escape(message)+"\",\""+ip+"\",\""+unescape(referer)+"\",\""+unescape(query)+"\"); return false;'>Details</a></td>";
	data=data+"	</tr>";
	data=data+"	<tr>";
	data=data+"		<td height='1'><img src='spacer.gif' alt='' width='147' height='1'></td>";
	data=data+"		<td><img src='spacer.gif' alt='' width='52' height='1'></td>";
	data=data+"		<td></td>";
	data=data+"		<td><img src='spacer.gif' alt='' width='113' height='1'></td>";
	data=data+"	</tr>";
	tblLog.innerHTML=tblLog.innerHTML+data;
}

function ShowDetails(date,type,message,ip,referer,query)
{
	var d=document;
	d.getElementById("LogDate").innerHTML=": "+date;
	d.getElementById("LogType").innerHTML=": "+type;
	d.getElementById("LogMessage").innerHTML=": "+unescape(message);
	d.getElementById("LogIP").innerHTML=": "+ip+" <a href='http://ip2location.com/"+ip+"' target='_blank'>Lookup IP Address</a>";
	d.getElementById("LogReferer").innerHTML=": <a href='"+referer+"' target='_blank'>"+referer+"</a>";
	d.getElementById("LogQueryString").innerHTML=": "+query+" - <a href='"+document.location+"?"+query+"' target='_blank'>Debug XML</a>";
	ShowHideLayers("LogDetails",'',"show");
}