var SettingsAjax = new Ajax("index.php","GET");
var Settings_sid=CreateStatus();

function LoadSettings()
{
	SetStatus('Updating settings...',Settings_sid);
	SettingsAjax.Go("Action=GetSettings");
}

function SaveSettings()
{
	vChunkSize=document.getElementById('ChunkSize').value;
	vPageSize=document.getElementById('PageSize').value;
	if(document.getElementById('Debug').checked)
		vDebug=1;
	else
		vDebug=0;
	if(vChunkSize=='')
	{
		alert("Please specify a chunk size");
		return;
	}
	if(vPageSize=='')
	{
		alert("Please specify a page size");
		return;
	}
	SetStatus("Saving Settings",Settings_sid);
	SettingsAjax.Go("Action=SaveSettings&ChunkSize="+vChunkSize+"&PageSize="+vPageSize+"&Debug="+vDebug);
}

SettingsAjax.onResponse=function(r)
{
	if(r.State==4)
	{
		xmldoc=r.XML;
		SetStatus('',Settings_sid);
		ShowMessages(xmldoc)
		var root=xmldoc.getElementsByTagName('settings');
		for(var i=0; i<root.length;i++)
		{
			document.getElementById(root[i].getAttribute('name')).value=root[i].getAttribute('value');
		}
	}
}