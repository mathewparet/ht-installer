<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load DeleteDir.inc.php without through index.php");
	}

	function DeleteDir($dirorfile,$self=false) 
	{
		if (is_dir($dirorfile)) 
		{ 
			$directory = $dirorfile;
			if(substr($dir, -1, 1) == "/")
					$directory = substr($directory, 0, strlen($directory) - 1);
			if ($handle = opendir("$directory")) 
			{
				while (false !== ($item = readdir($handle))) 
				{
					if ($item != "." && $item != "..") 
					{
						if (is_dir("$directory/$item")) 
						{ 
							if(!DeleteDir("$directory/$item",true))
								return false;
						} 
						else
						{ 
							if(!unlink("$directory/$item"))
								return false;
						}
					}
				}
				closedir($handle);
				if ($self == true) 
				{ 
					if(!rmdir($directory))
						return false;
				}
			}
		}
		else
		{
			return false;
		}
		return true;
	}

?>