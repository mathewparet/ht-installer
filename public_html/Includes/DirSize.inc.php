<?php
	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access DirSize.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load DirSize.inc.php without through index.php");
	}

	function DirSize($dirname)
	{
		global $SupportedArchives;
		foreach($SupportedArchives as $SupportedArchive)
		{
			$Ar[]=".".$SupportedArchive;
		}
		$Contents=MakeFileList($dirname,$Ar);
		$TotalSize=0;
		foreach($Contents as $Content)
		{
			$TotalSize=$TotalSize+filesize($Content);
		}
		return $TotalSize;
	}
?>