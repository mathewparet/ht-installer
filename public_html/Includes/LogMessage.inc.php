<?php

	function LogMessage($Message,$Type='info')
	{
		global $HTTP_SESSION_VARS,$HTTP_SERVER_VARS;
		$log=@fopen(DPX_HT_INSTALLER_PATH."/log.txt","a+");
		fwrite($log,"<log date='".date("F j, Y, g:i a",time())."' type='".$Type."' ip='".$_SERVER['REMOTE_ADDR']."' username='".$_SESSION['UserName']."' referer='".$_SERVER['HTTP_REFERER']."' query='".urlencode($_SERVER['QUERY_STRING'])."'><![CDATA[".$Message."]]></log>\n");
		fclose($log);
		return "<message type='".$Type."'><![CDATA[".$Message."]]></message>";
	}

?>