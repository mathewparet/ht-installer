			var AjaxInstall=new Ajax("index.php","GET");
			var Install_sid=CreateStatus();			
			AjaxInstall.onResponse=function(r)
			{
				if(r.State==4)
				{
					SetStatus('',Install_sid);
					var xmldoc=r.XML;
					ShowMessages(xmldoc);
					SetFocusXML(xmldoc);
				}
			}
			
			AjaxInstall.onError=function(e)
			{
				alert("Communication Error on Install: "+e);
				SetStatus('',Install_sid);
			}
			
			function InstallClick(PackName)
			{
				myTab.Show("InstallPackage");
				document.getElementById("InstallPackageList").value=PackName;
				document.getElementById("SubPath").value='';
			}
			
			function StartInstall()
			{
				pack=document.getElementById("InstallPackageList").value;
				extractpath=document.getElementById("ExtractPath").value;
				subp=document.getElementById("SubPath").value;
				if(pack=="===| Select Package |===")
				{
					alert("Please choose a package first!");
					return;
				}
				if(extractpath=="")
				{
					alert("Please enter ane Extract Path!!");
					return;
				}
				SetStatus("Installing package ("+pack+")",Install_sid);
				AjaxInstall.Go("Action=Install&Package="+pack+"&ExtractPath="+extractpath+"&SubPath="+subp);
			}
