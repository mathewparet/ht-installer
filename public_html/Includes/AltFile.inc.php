<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access AltFile.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load AltFile.inc.php without through index.php");
	}


	function AltFile( $file, $dir, $pre = null, $suf = '_', $delm = 1 )
	{
		if (!$suf && !$pre) 
		{
			die( "Wrong usage of AltFile() function! You either need to define a prefix or suffix." );
		}
		if (substr($dir,-1) != '/')
			$dir .= '/';
		if (!file_exists($dir.$file))
			return $file;
		clearstatcache();	
		$x = 1;	
		while( true )
		{
			if ($delm == 1) 
			{
				$cfile = ($pre != "" ? $pre.$x : ""). FileExt($file,1,1). ($suf != "" ? $suf.$x : "") .'.'.FileExt($file,1);
			}
			else 
			{ 
				$cfile = ($pre != "" ? $x.$pre : ""). FileExt($file,1,1). ($suf != "" ? $x.$suf : "") .'.'.FileExt($file,1);
			}
			if (!file_exists($dir.$cfile)) 
			{
				return $cfile;
			}
			$x++;
		}
	}

?>