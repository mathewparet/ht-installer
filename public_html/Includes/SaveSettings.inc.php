<?php

	function SaveSettingsAction()
	{
		global $_GET,$Config,$_SERVER;
		if(!is_numeric($_GET['ChunkSize']))
			$xmldoc.="<message type='error'><![CDATA[<b>Chunk Size</b> should be numeric in nature]]></message>";
		if(!is_numeric($_GET['PageSize']))
			$xmldoc.="<message type='error'><![CDATA[<b>Page Size</b> should be numeric in nature]]></message>";
		$_config_data="UserName=".$Config['UserName']."&Password=".$Config['Password']."&ChunkSize=".$_GET['ChunkSize']."&PageSize=".$_GET['PageSize']."&Debug=".$_GET['Debug'];
		if(!$xmldoc)
		{
			if(!ioncube_write_file("config.php",$_config_data,true,$_SERVER['HTTP_HOST']))
			{
				$xmldoc.=LogMessage("Your settings have been saved :)");
				foreach($_GET as $GetName => $GetValue)
				{
					$xmldoc.="<settings name='".$GetName."' value='".$GetValue."'/>";
				}
			}
			else
			{
				$xmldoc.=LogMessage("Your settings could not be saved! :(",'error');
				$xmldoc.=GetSettingsAction();
			}
		}
		else
		{
			$xmldoc.=LogMessage("Your settings could not be saved! :(",'error');
			$xmldoc.=GetSettingsAction();
		}
		return $xmldoc;
	}
	
	function GetSettingsAction()
	{
		global $Config;
		$xmldoc='';
		foreach($Config as $_ConfigName => $_ConfigValue)
		{
			if($_ConfigName!="UserName" && $_ConfigName!="Password")
				$xmldoc.="<settings name='".$_ConfigName."' value='".$_ConfigValue."'/>";
		}
		return $xmldoc;
	}

?>