			var AjaxDeletePackage=new Ajax("index.php","GET");
			var DeletePackage_sid=CreateStatus();			
			AjaxDeletePackage.onResponse=function(r)
			{
				if(r.State==4)
				{
					SetStatus('',DeletePackage_sid);
					var xmldoc=r.XML;
					ShowMessages(xmldoc);
					DisplayPackages(xmldoc);
				}
			}
			
			AjaxDeletePackage.onError=function(e)
			{
				alert("Communiction error on Delete Package: "+e);
				SetStatus('',DeletePackage_sid);
			}
			
			function Delete(Pname)
			{
				if(confirm("Are you sure you want to delete the package: "+Pname+" ? This operation cannot be undone!"))
				{
					SetStatus("Deleting ("+Pname+")",DeletePackage_sid);
					AjaxDeletePackage.Go("Action=DeletePackage&Package="+Pname);
				}
			}
			
			function DeleteCheckedPackages()
			{
				if(confirm("Are you sure you want to delete the selected packages? (Click OK if you want to delete the selected packages.) This operation cannot be undone!"))
				{
					//enumerate selected packages
					i=0;
					i++;
					delstr="";
					while(ptr=document.getElementById("Check"+i))
					{
						if(ptr=="undefined") break;
						if(ptr.checked)
							delstr=delstr+"&Package[]="+ptr.value;
						i++;
					}
					if(delstr)
					{
						SetStatus("Deleting",DeletePackage_sid);
						AjaxDeletePackage.Go("Action=DeleteCheckedPackages"+delstr);
					}
					else
						alert("No packages selected to delete! To select a package, tick mark the checkbox correspondig to the package.");
				}
			}
