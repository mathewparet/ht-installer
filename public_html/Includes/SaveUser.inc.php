<?php

	function SaveUserAction()
	{
		global $Config,$_GET,$_SERVER;
		if(strlen($_GET['UserName'])<5)
			$xmldoc.="<message type='error'><![CDATA[<b>UserName</b> should have atleast 5 characters!]]></message>";
		if(strlen($_GET['NewPassword'])<5)
			$xmldoc.="<message type='error'><![CDATA[<b>New Password</b> should have atleast 5 characters!]]></message>";
		if($_GET['NewPassword']!=$_GET['RePassword'])
			$xmldoc.="<message type='error'><![CDATA[<b>New Passwords</b> do not match!]]></message>";
		if($_GET['OldPassword']!=$Config['Password'])
			$xmldoc.="<message type='error'><![CDATA[<b>Current Password</b> is wrong!]]></message>";
		if($xmldoc)
		{
			$xmldoc.=LogMessage("User account could not be updated!! :(",'error');
			return $xmldoc;
		}
		$_config_data="UserName=".$_GET['UserName']."&Password=".$_GET['NewPassword']."&ChunkSize=".$Config['ChunkSize']."&PageSize=".$Config['PageSize']."&Debug=".$Config['Debug'];
		if(!ioncube_write_file("config.php",$_config_data,true,$_SERVER['HTTP_HOST']))
		{
			$_SESSION['UserName']=$_GET['UserName'];
			$_SESSION['Password']=$_GET['NewPassword'];
			$xmldoc.=LogMessage("User account updated :)");
		}
		else
			$xmldoc.=LogMessage("User account could not be updated!! :(",'error');
		return $xmldoc;
	}

?>