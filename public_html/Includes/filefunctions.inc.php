<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access filefunctions.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load filefunctions.inc.php without through index.php");
	}

function dir_size( $dir, $rsize = "" )
{
	$dir_info = read_dir($dir, 0);
	$size = $dir_info['size'];
	if ($rsize == 'kb')
		$size = $size / 1024;
	if ($rsize == 'mb')
		$size = $size / (1024*1024);
	if ($rsize == 'gb')
		$size = $size / (1024*1024*1024);
	return $size;
}

/*
 * Read Directory!
 * 
 * This a function that can be used to retrieve size, list of files and list of
 * sub-directories in a directory..
 * 
 * @param   string	  $dir 		the full directory path including the trailing slash at end
 * @param   string    $collect  By default all files and folder names are collected and returned.
 * 					      		If a folder contains thousands of files and directories, the script
 *					  	  		will consume a lot of memory in the variables, in such case this
 * 					  	  		argument must be set to 0.
 * @param   integer	  $fullpaths  Set this argument to 1 if you want the full path to files/subdirs
 * @param   integer   $subdirs    Whether or not it should include files from subdirectories
 * @param	integer   $rmdir      This is set to 1 when the call is made using our del_dir function
 * 					    		  In case its set to 1, subdirectories are listed before directories
 * 
 * Example Usage:
 * $dir_info = read_dir( '/home/user/public_html/myDirectory/' );
 * echo $dir_info['size'];
 * 
 * @return  The directory size in bytes or as specified in the param
 * 
 */
function read_dir( $dir, $collect = 1, $fullpaths = 0, $subdirs = 1, $rmdir = 0 )
{

	// add a trailing slash if it required..
	if (substr($dir,-1) != '/')
		$dir .= '/';

	// create arrays!
	$dirs = $files = array();
	
	// open the directory..
	if (!($handle = @opendir( $dir ))) {
		return false;
	}
	
	// read the dir..
	while( ($file = readdir($handle)) !== false )
	{
		// skip '..' and '.'
		if ($file != '..' AND $file != '.')
		{
			// if its a directory..
			if (is_dir($dir.$file) AND $subdirs == 1) {
				
				// generate the size of this sub-dir..
				$dir_info = read_dir( $dir.$file.'/', $collect, $fullpaths );
				$size += $dir_info['size'];
				
				if ($collect != 0)
				{
					if ($rmdir != 1) {
						// collect dir..
						$dirs[]  = $fullpaths == 1 ? $dir.$file : $file;
					}
					
					// merge the dirs and files array..
					$dirs  = array_merge( $dirs, $dir_info['dirs'] );
					$files = array_merge( $files, $dir_info['files'] );
					
					if ($rmdir == 1) {
						// collect dir..
						$dirs[]  = $fullpaths == 1 ? $dir.$file : $file;
					}
				}

			} else if (is_file($dir.$file)) {

				// get the size of the file..
				$size += filesize( $dir.$file );
				
				// collect file name..
				if ($collect != 0)
					$files[] = $fullpaths == 1 ? $dir.$file : $file;
			}
		}
	}
	
	// close the directory handle..
	@closedir( $handle );
	
	// clear stats cache of php..
	clearstatcache();	
	
	// return an array of size, directories ( sub-dirs included ) and files!
	return array( "size" => $size, "dirs" => $dirs, "files" => $files );
}



/*
 * File Information!
 * 
 * Returns information about a file using a set of functions. Hey, dont remind me to use
 * stat function instead, thats just too boring..
 * 
 * The following information is returned:
 * atime - the time the file was last accessed ( unix timestamp )
 * ctime - the time the file was last changed  ( unix timestamp )
 * mtime - the time the file was last modified ( unix timestamp )
 * size  - the file size ( in bytes )
 * group - the group ID of the file
 * inode - the inode number of the file
 * owner - the user ID of the owner of the file
 * perms - the permissions on the file
 * 
 * @param	string  $file - The full file path should be provided..
 *  
 * Example Usage:
 * $file = file_info("/dir/okay.gif");
 * if ($file['size'] > 500) {
 * 	 // do stuff..
 * }
 * 
 * @return  array  File information on success, FALSE on errors!
 * 
 */
function file_info( $file )
{
	// check for the file existance and the file type..
	if (!file_exists($file) OR filetype($file) != 'file') {
		return false;
	}

	// collect all the info..
	$f['atime'] = fileatime($file);
	$f['ctime'] = filectime($file);
	$f['mtime'] = filemtime($file);
	$f['size']  = filesize($file);	

	// these functions wont work on windows..
	if (IS_WIN != 1)
	{
		$f['group'] = filegroup($file);
		$f['inode'] = fileinode($file);
		$f['owner'] = fileowner($file);
		$f['perms'] = fileperms($file);
	}
	
	// clear stats cache..
	clearstatcache();
	
	// return all the collected info..
	return $f;
	
}



/*
 * Create Zip!
 * 
 * This function can be used to create a zip file. It requires the zlib module available with php.
 * 
 * @param   string  $ar_name  specify the output file.. might be a full path or just an archive name
 * @param   array   $files    an array with the file names (with path) which will be zipped
 * @param   array   $dirs     an array with the directories to include in the archive
 * @param   integer $rdata    set it to 1 if you want the function return the compressed data instead
 * 							  of writing it to a file
 * 
 * Example Usage:
 * 
 * To create a zip file with some files in it:
 * create_zip("backup.zip", array("file1.gif","file2.gif"));
 * 
 * To create a zip file with a directory and all its files and subdirs:
 * create_zip("backup.zip", array("file1.gif","file2.gif"), array("mybackupdir/"));
 * 
 * @return  bool/mixed  TRUE on success, FALSE on error OR compressed data if rdata is 1..
 * 
 */
function create_zip( $ar_name, $files = array(), $dirs = array(), $rdata = 0 )
{
	// we need the zlib..
	if (!extension_loaded('zlib')) {
		die( "Zlib is required for the zip file creation! gzcompress() function required.." );
	}
	
	// we need one array at least..
	if (!is_array($dirs) AND !is_array($files)) {
		return false;
	}
	
	if (!$ar_name AND $rdata != 1) {
		return false;
	}

	$data_store = array(); // array to store compressed data
	$ctrl_dir   = array(); // array to store central dir record
	$loffset    = 0;       // last offset
	
	// our dir
	$cur_dir = getcwd();
	
	// our dir
	$cur_dir = getcwd();
	
	// loop and include all the files in the main directory..
	foreach( $files as $file )
    {
		// prepare the 1st directory var..
		$dir1 = substr($file, 0, strrpos($file, '/'));
		$file = substr($file, strrpos($file, '/')+1);

		// chdir because all of it needs to be added to the main ..
		chdir($dir1);
			
		// read the file..
		$fp = @fopen($file,'r');
		$data = @fread($fp, filesize($file));
		@fclose($fp);

		// collect the zip data..
//		zip_collect_data($data, $file, &$data_store, &$ctrl_dir, &$loffset);
		zip_collect_data($data, $file, $data_store, $ctrl_dir, $loffset);
    }
	
	// include all the dirs..
	foreach( $dirs as $dird )
	{
		if (substr($dird, -1) == '/') {
			$dird = substr($dird, 0, -1);
		}
		
		// prepare the 1st directory var..
		$dir1 = substr($dird, 0, strrpos($dird, '/'));
		$dird = substr($dird, strrpos($dird, '/')+1);

		// chdir because all of it needs to be added to the main ..
		chdir($dir1);
		
		// use our other function to read the dir
		// and return the files/dirs list..
		$dir_info = read_dir($dird, 1, 1, 1);
		
		if (is_array($dir_info['files']))
		  	$files2 = array_merge($files2, $dir_info['files']);
	}
	
	// loop and include all the files in SUB-Directories..
	if (is_array($files2)) {
		foreach( $files2 as $file )
    	{		
			// read the file..
			$fp = @fopen($file,'r');
			$data = @fread($fp, filesize($file));
			@fclose($fp);

			// collect the zip data..
//			zip_collect_data($data, $file, &$data_store, &$ctrl_dir, &$loffset);
			zip_collect_data($data, $file, $data_store, $ctrl_dir, $loffset);
    	} // end loop
	}
	
	// join data_store in a variable..
	$data = implode("", $data_store );  
	$ctrldir = implode("", $ctrl_dir );  
	
	// prepare the final data to be written..
	$file_data = $data. $ctrldir. "\x50\x4b\x05\x06\x00\x00\x00\x00" . pack("v", sizeof($ctrl_dir)) . pack("v", sizeof($ctrl_dir)).
				 pack("V", strlen($ctrldir)) . pack("V", strlen($data)). "\x00\x00";
	
	// if we need to return data instead of writing it to a file..
	if ($rdata == 1) {
		return $file_data;
	}
	
	// change to the working dir..
	chdir($cur_dir);
	
	// open the file for writing ..
	if (!($fp = fopen($ar_name, 'wb'))) {
		return false;
	}

	// write to the file..
	fwrite($fp, $file_data);
	fclose($fp);
	
	return true;
}

/*
 * Create Tar Files!
 * 
 * This function can be used to create UNCOMPRESSED tar files on the fly :) .. Some ideas were taken 
 * off Pear's tar class.
 * 
 * @param   string  $ar_name  specify the output file.. might be a full path or just an archive name
 * @param   string  $type     CURRENTLY NOT IMPLEMENTED..
 * @param   array   $files    an array with the file names (with path) which will be zipped
 * @param   array   $dirs     an array with the directories to include in the archive
 * @param   integer $rdata    set it to 1 if you want the function return the compressed data instead
 * 							  of writing it to a file
 * 
 * Example Usage:
 * 
 * To create a tar file with some files in it:
 * create_tar("backup.tar", array("file1.gif","file2.gif"));
 * 
 * To create a tar file with a directory and all its files and subdirs:
 * create_tar("backup.tar", array("file1.gif","file2.gif"), array("mybackupdir/"));
 * 
 * @return  bool/mixed  TRUE on success, FALSE on error OR compressed data if rdata is 1..
 * 
 */
function create_tar( $ar_name, $type = 'tar', $files = array(), $dirs = array(), $rdata = 0 )
{
	// we need one array at least..
	if (!is_array($dirs) AND !is_array($files)) {
		return false;
	}
	
	// we need an archive name or the return data to be set to 1..
	if (!$ar_name AND $rdata != 1) {
		return false;
	}
	
	// our current working dir!
	$cur_dir = getcwd();
	
	// include all the dirs..
	foreach( $dirs as $dird )
	{
		if (substr($dird, -1) == '/') {
			$dird = substr($dird, 0, -1);
		}
		
		// prepare the 1st directory var..
		$dir1 = substr($dird, 0, strrpos($dird, '/'));
		$dird = substr($dird, strrpos($dird, '/')+1);

		// chdir because all of it needs to be added to the main ..
		chdir($dir1);
			
		// use our other function to read the dir
		// and return the files/dirs list..
		$dir_info = read_dir($dird, 1, 1, 1);
		
		if (is_array($dir_info['files']))
		  	$files = array_merge($files, $dir_info['files']);
	}

	// loop and include all the files..
	foreach( $files as $file )
    {
		// read the file..
		$fp = fopen($file,'r');
		$data = fread($fp, filesize($file));
		fclose($fp);
		
		// file type..
		$f_type = 0;
		
		// get the file information..
		$f_inf = file_info( $file );

		// prepare some variables..
		$size = sprintf("%11s ", decoct(strlen($data)));
		$perm  = sprintf("%6s ", decoct($f_inf['perms']));
		$uid   = sprintf("%6s ", decoct($f_inf['uid']));
		$gid   = sprintf("%6s ", decoct($f_inf['gid']));
		$mtime = sprintf("%11s ", decoct($f_inf['mtime']));		

		// create the first header with file, permissions, uid, gid, size and msdostime..
		$data_1 = pack("a100a8a8a8a12a12", $file, $perm, $uid, $gid, $size, $mtime);

		// create the second header... we aren't going to define anything over here..
		$data_2 = pack("a1a100a6a2a32a32a8a8a155a12", $f_type, "", "", "", "unknown", "unknown", "", "", "", "");

		// set checksum to 0.. 
		$checksum = 0;

		// first part of the header..
		for ($i = 0; $i < 148; $i++)
		{
			$checksum += ord(substr($data_1, $i, 1));
		}
		
		for ($i = 148; $i < 156; $i++)
		{
			$checksum += ord(' ');
		}

		// last part of the header..
		for ($i = 156, $j = 0; $i < 512; $i++, $j++)
		{
			$checksum += ord(substr($data_2, $j, 1));
		}

		// format checksum..
		$checksum = sprintf("%6s ", decoct($checksum));
		
		// start preparing output..
		$output = $data_1;
		$output .= pack( "a8", $checksum ) . $data_2 . $data;
		
		// file size again..		
		$size = strlen($data);
		
		// a tar header is 512 bytes, so keep adding null bytes to make it 512...
		if ($size > 0 AND ($size % 512) != 0) {
			$output .= str_repeat("\0", 512 - ($size % 512));
		}

		// add it to file_data..
		$file_data .= $output;

		// unset..
		unset($output);		
    }
	
	// tar file footer..
	$file_data .= pack( "a512", "" );
	
	// if we need to return data instead of writing it to a file..
	if ($rdata == 1) {
		return $file_data;
	}
	
	// change to the working dir..
	chdir($cur_dir);
	
	// open the file for writing ..
	if (!($fp = fopen($ar_name, 'wb'))) {
		return false;
	}

	// write to the file..
	fwrite($fp, $file_data);
	fclose($fp);
	
	return true;
}

/*
 * Extract Zip Files!
 * 
 * I have been searching for something similar for a while and couldn't find one that suits my need. 
 * There is something called PCLZip or whatever it is, but that just is too messed up and will never
 * help someone understand how zip files are extracted. 
 * 
 * I have created this function as simple as possible. Its very easy to understand and use :)
 * 
 * Official zip format: http://www.pkware.com/company/standards/appnote/index.php
 * 
 * @param   string  $file    full path to the zip file which needs to be extracted..
 * @param   string  $ex_dir  you can specify an output directory if you wish..
 * 
 * Example Usage:
 * ex 1:
 * extract_zip("file.zip");
 * ex 2:
 * extract_zip("file.zip", "mydirectory/");
 * 
 * @return  bool  TRUE on success, FALSE on error
 * 
 */



//----------------------------------------------------
// !! MEMBER FUNCTIONS !!
// Below are the member functions of the functions 
// defined above..
//----------------------------------------------------

/*
 * Zip file collection and compression!
 * 
 * This function is a member function of create_zip, used to collect and compress data.
 * Some of the code is based on a script that I found somewhere long ago by Eric Mueller..
 * 
 * @param   mixed   $data   file data that needs to be compressed
 * @param   string  $name   name of the file that is being compressed
 * @param   pointer $data_store  a pointer to an array that will be used to collect compressed data
 * @param   pointer $ctrl_dir    a pointer to the ctrl_dir array that will be used to collect ctrl_dir
 * 								 related data..
 * @param   pointer $loffset     last offset pointer
 * 
 * Example Usage:
 * Not available..
 * 
 * @access  private
 * @see     create_zip()
 */
function zip_collect_data($data, $name, &$data_store, &$ctrl_dir, &$loffset)
{
	$name = str_replace("\\", "/", $name);
	
	// file header sig..
	$fr   = "\x50\x4b\x03\x04";
	// version needed to extract the file..
	$fr   .= "\x14\x00" . "\x00\x00";
	// compression method..
	$fr   .= "\x08\x00";
	
	// prepare dos time..
    $d_time = date_unix2dos( time() );
	$dtime  = pack('v', $d_time[1]);
	$dtime .= pack('v', $d_time[0]);
	
	// add the modification date/time header..
	$fr   .= $dtime;

	// "local file header" segment
	$unc_len = strlen($data);
	$crc     = crc32($data);
	$zdata   = gzcompress($data,9);
	$zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2); // fix crc bug
	$c_len   = strlen($zdata);
	$fr      .= pack('V', $crc);             // crc32
	$fr      .= pack('V', $c_len);           // compressed filesize
	$fr      .= pack('V', $unc_len);         // uncompressed filesize
	$fr      .= pack('v', strlen($name));    // length of filename
	$fr      .= pack('v', 0);                // extra field length
	$fr      .= $name;

	// "file data" segment
	$fr .= $zdata;

	// "data descriptor" segment (optional but necessary if archive is not served as file)
	$fr .= pack('V', $crc);                 // crc32
	$fr .= pack('V', $c_len);               // compressed filesize
	$fr .= pack('V', $unc_len);             // uncompressed filesize

	// add this entry to array
	$data_store[] = $fr;
	$new_offset   = strlen(implode('', $data_store));

	// now add to central directory record
	$cdrec = "\x50\x4b\x01\x02";
	$cdrec .= "\x00\x00";                // version made by
	$cdrec .= "\x14\x00";                // version needed to extract
	$cdrec .= "\x00\x00";                // gen purpose bit flag
	$cdrec .= "\x08\x00";                // compression method
	$cdrec .= $dtime;        			 // last mod time & date
	$cdrec .= pack('V', $crc);           // crc32
	$cdrec .= pack('V', $c_len);         // compressed filesize
	$cdrec .= pack('V', $unc_len);       // uncompressed filesize
	$cdrec .= pack('v', strlen($name));  // length of filename
	$cdrec .= pack('v', 0);              // extra field length
	$cdrec .= pack('v', 0);              // file comment length
	$cdrec .= pack('v', 0);              // disk number start
	$cdrec .= pack('v', 0);              // internal file attributes
	$cdrec .= pack('V', 32 );            // external file attributes - 'archive' bit set

	$cdrec .= pack('V', $loffset); // relative offset of local header
	$loffset = $new_offset;

	$cdrec .= $name;

	// optional extra field, file comment goes here save to central directory
	$ctrl_dir[] = $cdrec;
}

/*
 * Unix Date/Time conversion to Dos
 * 
 * Zip files use the msdos based time! This functions is created to serve the unix-msdos 
 * date and time conversion needs.
 * 
 * @param	string  $timestamp  a unix timestamp
 * 
 * Example Usage:
 * $dtime = date_unix2dos(time());
 * 
 * @return  array  [0] => dos date, [1] => dos time
 * @see     create_zip()
 */
function date_unix2dos( $timestamp )
{
	if (!$timestamp) {
		$timestamp = time();
	}
	
	// get date ...
	$date = getdate($timestamp);
	$hour = $date['hours'];

	// Lsh 11 on hour.. ( hour * (2^11) )
	$hour = $hour << 11;
	
	// Lsh 5 on mins.. ( mins * (2^5) )
	$mins = $date['minutes'] << 5;
	
	// divide seconds by 2
	$secs = round($date['seconds'] / 2);

	// generate ms-dos based time!
	$time = $hour + $mins + $secs;
	
	// return [0] => date, [1] => time ..
	return array( ((($date['year']-1980) * 512) + ($date['mon'] * 32) + $date['mday']), $time );
}


?>