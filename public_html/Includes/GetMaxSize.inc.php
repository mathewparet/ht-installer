<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load GetMaxSize.inc.php without through index.php");
	}

	function GetMaxSize()
	{
		$post_max=ini_get("post_max_size");
		$upload_max=ini_get("upload_max_filesize");
		$post=substr($post_max,0,strpos($post_max,"M"));
		$upload=substr($upload_max,0,strpos($upload_max,"M"));
		$post*=1024*1024;
		$upload*=1024*1024;
		if($post>$upload) return $upload;
		else return $post;
	}

?>