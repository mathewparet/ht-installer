<?php
	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load HTTPDownload.inc.php without through index.php");
	}

	function HTTPDownloadAction()
	{
		global $_GET;
		$xmlData.=HTTPDownload($_GET['PackageURL']);
		$xmlData.=UpdatePackage();
		return $xmlData;
	}
	
	function ContinueHTTPDownloadAction()
	{
		global $_GET;
		$xmlData.=HTTPDownload();
		$xmlData.=UpdatePackage();
		return $xmlData;
	}
	
	
	function FsockFileDownload($File,$Chunk)
	{
		$url=parse_url($File);
		if($url['scheme']=='')
			return "<message type='error'>Protocol not specified.</message>";
		$DFunc=strtoupper($url['scheme']."FsockFileDownload");
		if(function_exists(strtoupper($DFunc)))
		{
			return $DFunc($File,$Chunk);
		}
		else
		{
			LogMessage("Protocol <b>(".strtoupper($url['scheme']).")</b> not supported yet.",'error');
			return "<message type='error'><![CDATA[Protocol <b>(".strtoupper($url['scheme']).")</b> not supported yet.]]></message>";
		}
	}
	
	function HTTPDownload($URL='')
	{
		global $_SESSION,$Config;
		$xmlData='';
		clearstatcache();
		$Chunk=$Config['ChunkSize'];	//	1024*1024
		if($URL=='')
			$URL=$_SESSION['URL'];
		else
			$_SESSION['URL']=$URL;
		if($URL=='')
		{
			$xmlData="<message type='error'>Please enter the url of the package you wish to download!</message>";
			return $xmlData;
		}
		if($_SESSION['FilePos']=='' || !isset($_SESSION['FilePos']))
		{
			$_SESSION['FilePos']=0;
			$_SESSION['FileNumber']=0;
		}
		if($_SESSION['FileStatus']=='Completed')	//	file donwload completed, now merge the files
		{
				unset($_SESSION['URL']);
				unset($_SESSION['FilePos']);
				unset($_SESSION['FileSize']);
				unset($_SESSION['FileStatus']);
				unset($_SESSION['HTIResume']);
				copy(DPX_HT_INSTALLER_PATH."/_temp/".$_SESSION['FileRName'],DPX_HT_INSTALLER_PATH."/packages/".$_SESSION['FileRName']);
				unlink(DPX_HT_INSTALLER_PATH."/_temp/".$_SESSION['FileRName']);
				$msg= "<message type='info'><![CDATA[Package downloaded as <b>".$_SESSION['FileRName']."</b> :)]]></message>";
				unset($_SESSION['FileRName']);
				unset($_SESSION['FileRName']);
				return $msg;
		}
		// start downloading
		$message=FsockFileDownload(urldecode($URL),$Chunk);
		if($message!="")
		{
			unset($_SESSION['URL']);
			unset($_SESSION['FilePos']);
			unset($_SESSION['FileSize']);
			unset($_SESSION['FileStatus']);
			unset($_SESSION['HTIResume']);
			unset($_SESSION['FileName']);
			unset($_SESSION['FileRName']);
			return $message;
		}
		if($_SESSION['HTIResume']=="true")
		{
			if($_SESSION['FileSize']!=$_SESSION['FilePos'])
				return "<status value='ContinueHTTPDownload'>Downloading package (".$_SESSION['FileName'].")... ".perfectsize($_SESSION['FilePos'])." of ".perfectsize($_SESSION['FileSize'])." </status>";
			else
				return "<status value='ContinueHTTPDownload'>Saving package (".$_SESSION['FileName'].")on server... </status>";
		}
		else
			return "<status value='ContinueHTTPDownload'>Downloading package (".$_SESSION['FileName'].") [ !! Resume Not Supported !! ]</status>";
	}
?>