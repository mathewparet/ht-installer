<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load ErrorHandler.inc.php without through index.php");
	}

	function ErrorHandler($errno,$errmsg,$filename,$lineno,$vars)
	{
		global $errXML;
		if($errno!=8)
		{
//			$errXML.="\t<message type='error'>Error No: ".$errno.", ".substr($errmsg,strpos($errmsg,"]:")+2)."</message>\n";
			LogMessage($errmsg,"php");	
		}
	}
	
	set_error_handler("ErrorHandler");

?>