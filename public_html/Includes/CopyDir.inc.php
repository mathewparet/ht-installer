<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access CopyDir.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load CopyDir.inc.php without through index.php");
	}


	function CopyDir( $dir, $copy_to )
	{
		if (!is_dir($copy_to)) 
		{
			if (!mkdir($copy_to, 0777)) 
			{
				return false;
			}
		}
		@chmod($copy_to, 0777);
		if (!is_dir($dir)) 
		{
			return false;
		}
		if (substr($copy_to, -1) != '/') 
		{
			$copy_to .= '/';
		}
		if (substr($dir, -1) != '/') 
		{
			$dir .= '/';
		}
		$dir_info = read_dir($dir, 1, 1);
		foreach($dir_info['dirs'] as $dirs) 
		{
			$dirs = substr($dirs, strpos($dirs, $dir) + strlen($dir));
			$last = "";
			$c_dir = explode( "/", $dirs );
			foreach($c_dir as $tdir) 
			{
				if(!is_dir($copy_to. $last .$tdir)) 
				{
					mkdir($copy_to. $last .$tdir, 0777 );
				}
				$last .= $tdir . '/';
			}
			if (!is_dir($copy_to.$dirs)) 
			{
				mkdir($copy_to.$dirs, 0777);
			}
			@chmod($copy_to.$dirs, 0777);
		}
		foreach($dir_info['files'] as $file )
		{
			$fn = basename($file);  	// get the file name
			$cdir = dirname($file); 	// get the directory name
			$cdir = substr($cdir, strpos($cdir, $dir) + strlen($dir));
			if (substr($cdir, -1) != '/') 
			{
				$cdir .= '/';
			}
			@copy($file, $copy_to.$cdir.$fn);
		}
		return true;
	}

?>