var Response=new Object();
var RS_UNINITIALIZED=0;
var RS_LOADING=1;
var RS_FINISHED_LOADING=2;
var RS_INITIALIZING=3;
var RS_INITIALIZED=4

function Ajax(Page,Method)
{
	this.Page=Page;
	this.Method=Method;
}

Ajax.prototype.Go=function(Parameters)
{
	this.Params=Parameters;
	_DoAjax(this.Page,this.Method,this.Params,this.onResponse,this.onError);
}

function _DoAjax(_Page,_Method,_Params,_Response,_Error)
{
	var request=false;
	try
	{
		request = new XMLHttpRequest();
		if (request.overrideMimeType) {
            request.overrideMimeType('text/xml');
		}
	}
	catch (trymicrosoft)
	{
		try
		{
			request = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (othermicrosoft)
		{
			try
			{
				request = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (failed) 
			{
				request = false;
			}  
     	}
   	}
	if(!request)
		alert("Error: Couldn't initialize!!");
	if(_Method=='GET')
	{
		request.open(_Method,_Page+"?"+_Params,true);
		request.send(null);
	}
	else if(_Method=='POST')
	{
		request.open(_Method,_Page,true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.setRequestHeader("Content-length", _Params.length);
		request.setRequestHeader("Connection", "close");
		request.send(_Params);
	}
	else
		alert("Method '"+_Method+"' not recognized. Please use 'POST' or 'GET'");
	request.onreadystatechange=function()
	{
		if(request.readyState==4 && request.status!=200)
			_Error(request.status);
		else
		{
			Response.State=request.readyState;
			Response.Text=request.responseText;
			Response.XML=request.responseXML;
			_Response(Response);
		}
	}

}