// JavaScript Document

function Tab(TabsDiv,ActiveClass,InactiveClass,DescDiv,MyName)
{
	this._Tabs=new Array();
	this.DescDiv=DescDiv;
	this.TabsDiv=TabsDiv;
	this.ActiveClass=ActiveClass;
	this.InactiveClass=InactiveClass;
	this.LastTab='';
	this.MyName=MyName;
}

Tab.prototype.Add=function(TabName,TabTitle,TabDesc,TabDiv)
{
	this._Tabs.push(TabName);
	this._Tabs.push(TabTitle);
	this._Tabs.push(TabDesc);
	this._Tabs.push(TabDiv);
}


Tab.prototype.Find=function(TabName)
{
	for(var i=0;i<this._Tabs.length;i=i+4)
	{
		if(this._Tabs[i]==TabName)
		{
			// this is the tab to be displayed
			return i;
		}
	}

}

Tab.prototype.Show=function(TabName)
{
	if(this.onClick(TabName)==1)
	{
		var newTabID=this._Tabs[this.Find(TabName)];	//	nd tabs name
		var newTabDesc=this._Tabs[this.Find(TabName)+2];	//	new tabs desc
		var newTabDispID=this._Tabs[this.Find(TabName)+3];	// new tabs diaplay div
		if(this.LastTab!='')
		{
			var mTabID=this._Tabs[this.Find(this.LastTab)];
			var mTabDispID=this._Tabs[this.Find(this.LastTab)+3];
			document.getElementById(mTabID).setAttribute("Class",this.InactiveClass);
			document.getElementById(mTabDispID).style.display="none";
		}
		this.LastTab=TabName;
		document.getElementById(newTabID).setAttribute("Class",this.ActiveClass);
		document.getElementById(newTabDispID).style.display="block";
		document.getElementById(this.DescDiv).innerHTML=newTabDesc;
	}
	this.onShow(TabName);
}

Tab.prototype.Install=function(DefTab)
{
	var strCode='';
	for(var i=0;i<this._Tabs.length;i=i+4)
	{
		strCode=strCode+"<span class='"+this.InactiveClass+"' id='"+this._Tabs[i]+"'><a href='#' onClick='javascript:"+this.MyName+".Show(\""+this._Tabs[i]+"\"); return false;'>"+this._Tabs[i+1]+"</a></span>";
		document.getElementById(this._Tabs[i+3]).style.display="none";
	}
	document.getElementById(this.TabsDiv).innerHTML=strCode;
	this.Show(DefTab);
}