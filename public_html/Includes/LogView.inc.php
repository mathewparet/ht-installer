<?php
	
	function ParseLogEntriesAction()
	{
		global $_GET,$Config;
		$xmlData.='';
		$PageSize=$Config['PageSize'];	//10
		$Start=$_GET['Start'];
		if($Start=='')	$Start=0;
		if(!$Start)	$PageSize++;
		$Next=$Start+$PageSize;
		if(file_exists("log.txt"))
		{
			$LogContents=file_get_contents("log.txt");
			$Log=explode("\n",$LogContents);
			$Log=array_reverse($Log);
			for($i=$Start;$i<$Start+$PageSize;$i++)
			{
				$xmlData.=$Log[$i]."\n";
			}
		}
		$xmlData.="<var name='Start' value='".$Next."'/>";
		$xmlData.="<var name='PageSize' value='".$PageSize."'/>";
		return $xmlData;
	}	
	
	function LogFileSizeAction()
	{
		if(file_exists(DPX_HT_INSTALLER_PATH."/log.txt"))
			$xmlData.="<var name='FileSize' value='".perfectsize(filesize(DPX_HT_INSTALLER_PATH."/log.txt"))."'/>";
		else
			$xmlData.="<var name='FileSize' value='Not Found!'/>";
		return $xmlData;
	}
	
	function ClearLogEntriesAction()
	{
		unlink(DPX_HT_INSTALLER_PATH.'/log.txt');
		LogMessage("Log File Cleared");
		return ParseLogEntriesAction();
	}
?>