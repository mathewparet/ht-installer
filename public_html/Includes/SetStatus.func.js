function SetStatus(message,ID)
{
		if(message!='')
		{
			StatusInfo[ID]=message;
			message=message+"... <img src='progress.gif'>";
			document.getElementById("msg").innerHTML="";
		}
		else
		{
			StatusInfo[ID]='';
			for(var i=0;i<StatusInfo.length;i++)
			{
				if(StatusInfo[i]!='' && StatusInfo[i]!='UNDEFINED')
					message=StatusInfo[i];
			}
			if(message=='')
				message="<a href=\"#\" onClick=\"javascript: UpdatePackageList(); return false;\">[ Refresh ]</a> <a href='#' onClick='javascript: document.getElementById(\"msg\").innerHTML=\"\"; return false;'>[Clear Messages]</a>";
		}
		document.getElementById("Status").innerHTML=message;
		ShowStatusInfo();
}

function CreateStatus()
{
	StatusInfo.push("UNDEFINED");
	return StatusInfo.length;
}

function ShowStatusInfo()
{
	var st=''
	document.getElementById("StatusDisplayDiv").innerHTML='';
	for(var i=0; i<StatusInfo.length;i++)
	{
		if(StatusInfo[i]!='' && StatusInfo[i]!='UNDEFINED')
			st=st+"<li>"+StatusInfo[i]+"&nbsp;<img src='progress.gif'></li>";
	}
	if(st=='')
		st='<center>HT Installer is currently idle.</center>';
	document.getElementById("StatusDisplayDiv").innerHTML=st;
}