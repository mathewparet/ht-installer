var AjaxHTTPDownload=new Ajax("index.php","GET");
var HTTPDownload_sid=CreateStatus();
AjaxHTTPDownload.onResponse=function(r)
{
	if(r.State==4)
	{
		SetStatus('',HTTPDownload_sid);
		var xmldoc=r.XML;
		ShowMessages(xmldoc);
		var root=xmldoc.getElementsByTagName("status");
		if(root.length)
		{
			status=root[0].getAttribute("value");
			SetStatus(root[0].childNodes[0].nodeValue,HTTPDownload_sid);
			if(status!='')
			{
				AjaxHTTPDownload.Go("Action="+status);
			}
		}
		else
			DisplayPackages(xmldoc);
	}
}

AjaxHTTPDownload.onError=function(e)
{
	alert("Communication error on HTTP Download: "+e);
	SetStatus('',HTTPDownload_sid);
}

function AjaxStartDownload()
{
	path=document.getElementById("DownloadURL").value;
	document.getElementById("DownloadServerID").style.display="none";
	SetStatus("Preparing to download to server",HTTPDownload_sid);
	AjaxHTTPDownload.Go("Action=HTTPDownload&PackageURL="+escape(path));
}

function DownloadToServer()
{
	document.getElementById("DownloadServerID").style.display="block";
}
