<?php
	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load makefilelist.inc.php without through index.php");
	}

	function MakeFileList($dir, $mask, $nomask = array('.', '..', 'CVS'), $recurse = TRUE, $min_depth = 0, $depth = 0) 
	{
		$key = (in_array($key, array('filename', 'basename', 'name')) ? $key : 'filename');
		$files = array();
		$dir=substr($dir,0,strlen($dir)-1);
		if (is_dir($dir) && $handle = opendir($dir)) 
		{
			while ($file = readdir($handle)) 
			{
				if (!in_array($file, $nomask) && $file[0] != '.') 
				{
					if (is_dir("$dir/$file") && $recurse) 
					{
						$files = array_merge($files, MakeFileList("$dir/$file/", $mask, $nomask, $recurse, $min_depth, $depth + 1));
					}
					elseif ($depth >= $min_depth) 
					{
						foreach($mask as $msk)
							if(ereg($msk, $file))
							{
								$filename = "$dir/$file";
								$basename = basename($file);
								$name = substr($basename, 0, strrpos($basename, '.'));
								$files[] =$filename;
								break;
							}
					}
				}
			}
		closedir($handle);
		}
		return $files;
	}
?>