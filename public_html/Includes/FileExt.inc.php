<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access FileExt.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load FileExt.inc.php without through index.php");
	}


function FileExt( $file, $way = 1, $fwe = 0 )
{
	if ($way == 1) 
	{
		if (preg_match('/^(.+?)\.([a-z0-9]{1,4})$/', $file, $ext)) 
		{
				return ($fwe == 1 ? $ext[1] : $ext[2]);
		} 
		else 
		{
				return false;
		}
	} 
	else if ($way == 2 ) 
	{
		if (($pos = strrpos($file,'.')) !== false) 
		{
				return ($fwe == 1 ? substr($file,0,$pos) : substr($file, $pos+1));
		}
		else 
		{
				return false;
		}
	}	
}

?>