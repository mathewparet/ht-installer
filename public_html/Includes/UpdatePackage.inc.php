<?php
	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load UpdatePackage.inc.php without through index.php");
	}

	function UpdatePackageListAction()
	{
		$xmlData.=UpdatePackage();
		return $xmlData;
	}

	function UpdatePackage()
	{
		global $SupportedArchives;
		$_packages=MakeFileList(DPX_HT_INSTALLER_PATH."/packages/",$SupportedArchives);
		$retdata="\t\t<packages>\n";
		foreach($_packages as $_package)
		{
			$retdata.="\t\t\t<package name='".basename($_package)."' size='".perfectsize(filesize($_package))."' date='".date("F j, Y, g:i a",filemtime($_package))."'/>\n";
		}
		$retdata.="\t\t</packages>\n";
		return $retdata;
	}
	
	function TotalPackageSizeAction()
	{
		$xmlData.=UpdatePackage();
		$xmlData.="<var name='TotalPackageSize' value='".perfectsize(DirSize(DPX_HT_INSTALLER_PATH."/packages/"))."'/>";
		return $xmlData;
	}

?>