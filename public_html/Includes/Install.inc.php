<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load Install.inc.php without through index.php");
	}

	function InstallAction()
	{
		global $_GET;
		$xmlData.=Install($_GET['Package'],$_GET['ExtractPath'],$_GET['SubPath']);
		$xmlData.=UpdatePackage();
		return $xmlData;
	}

	function Install($Package,$Extractpath,$SubPath)
	{
		if(!file_exists(DPX_HT_INSTALLER_PATH."/packages/".$Package))
		{
			$xmlData.="<message type='error'><![CDATA[File not found: <b>".$Package."</b>]]></message>";
			return $xmlData;
		}
		if(!is_dir($Extractpath))
		{
			$last = "";
			$c_dir = explode( "/", $Extractpath );
			foreach($c_dir as $dir) 
			{
				if(!is_dir($last .$dir))
				{
					mkdir($last .$dir, 0777 );
				}
				$last .= $dir . '/';
			}
		}
		$ArchiveHandler=strtoupper(FileExt($Package))."ArchiveExtract";
		if(function_exists($ArchiveHandler))
		{
			if($ArchiveHandler(DPX_HT_INSTALLER_PATH."/packages/".$Package,DPX_HT_INSTALLER_PATH."/_temp/",$SubPath))
			{
				$xmlData.="<message type='info'><![CDATA[Package <b>(".$Package.")</b> successfully extracted :)]]></message>";
				CopyDir(DPX_HT_INSTALLER_PATH."/_temp",$Extractpath);
				$xmlData.="<message type='info'><![CDATA[Package <b>(".$Package.")</b> successfully installed :)]]></message>";
				if(DeleteDir(DPX_HT_INSTALLER_PATH."/_temp"))
					$xmlData.="<message type='info'>Temporary files successfully cleared.</message>";
				else
					$xmlData.="<message type='error'>Temporary files could not be cleared.</message>";
				LogMessage("Package <b>".$Package."</b> successfully installed :)");
			}
			else
				$xmlData.="<message type='error'><![CDATA[Package <b>(".$Package.")</b> could not be extracted!]]></message>";
			$xmlData.="<focus name='InstallPackageList' value='".$Package."'/>";
			$xmlData.="<focus name='ExtractPath' value='".$Extractpath."'/>";
		}
		else
		{
			$xmlData.="<message type='error'><![CDATA[Archive type <b>(".strtoupper(FileExt($Package)).")</b> not supported yet!]]></message>";
		}
		return $xmlData;
	}

?>