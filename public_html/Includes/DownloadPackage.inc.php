<?php
	
	include_once "dl.class.php";
	
	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access DownloadPackage.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load DownloadPackage.inc.php without through index.php");
	}

	function downloadAction()
	{
		global $_SERVER;
		$_Request=substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],"index.php/")+strlen("index.php/"));
		$Request=explode("/",$_Request);
		if($Request[0]=="download")
		{
			if(file_exists(DPX_HT_INSTALLER_PATH."/packages/".urldecode($Request[1])))
			{
				DownloadPackage(urldecode($Request[1]));
			}
			else
			{
				header("HTTP/1.0 404 Not Found");
				LogMessage("Wrong download request (file doesn't exist): <b>".urldecode($Request[1])."</b>","error");
				die();
			}
			exit;
		}
	}

	function DownloadPackage($Package)
	{
		$dlpack=new downloader;
		$dlpack->set_byfile(DPX_HT_INSTALLER_PATH."/packages/".$Package);
		$dlpack->use_resume=true;
		$dlpack->filename=$Package;
		$dlpack->mime="application/zip";
		$dlpack->download();
		LogMessage("Package <b>".$Package."</b> downloaded!");
	}

?>