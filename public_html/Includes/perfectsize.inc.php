<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load perfectsize.inc.php without through index.php");
	}

	
	function perfectsize($Size)
	{
		if($Size<1024)
			return $Size." bytes";
		if($Size>=1024 && $Size < (1024*1024))
			return (round($Size/1024,2))." Kb";
		else
			return (round($Size/(1024*1024),2)). " Mb";
	}


?>