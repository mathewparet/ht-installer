<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load ListContents.inc.php without through index.php");
	}

	function ListContentsAction()
	{
		global $_GET;
		$xmlData.=ListContents($_GET['Package'],$_GET['Home']);
		return $xmlData;
	}

	function ListContents($Package,$Home='')
	{
		$ArchiveHandler=strtoupper(FileExt($Package))."ArchiveContents";
		if(!function_exists($ArchiveHandler))
		{
			$xmlData.="<message type='error'><![CDATA[Package type <b>(".strtoupper(FileExt($Package)).")</b> not supported!]]></message>";
		}
		else
		{
			$contents=$ArchiveHandler(DPX_HT_INSTALLER_PATH.'/packages/'.$Package);
			foreach($contents as $content)
			{
				$dir_name  = dirname($content['name']);    // get the directory name
				if($dir_name!='.')
				{
					if($Home!='')
					{
						if(strpos($dir_name,$Home)!==false)
						{
							if(strlen($dir_name)>=(strlen($Home)+1))
							{
								if(substr($dir_name,0,strpos($dir_name,"/",strlen($Home)+1))!='')
								{
									$folders[]=substr($dir_name,0,strpos($dir_name,"/",strlen($Home)+1));
								}
							}
						}
					}
					else
					{
						if(strpos($dir_name,"/")!==false)
							$dir_name=substr($dir_name,0,strpos($dir_name,"/"));
						$folders[]=$dir_name;
					}
				}
			}
			if(is_array($folders))
				$folders=array_unique($folders);
			if($Home!='')
				$xmlData.="<subfile name='".substr($Home,0,strrpos($Home,"/"))."' type='home'/>";
			foreach($folders as $folder)
			{
				$xmlData.="<subfile name='".$folder."' type='folder'/>";			
			}
		}
		$xmlData.="<focus name='InstallPackageList' value='".$Package."'/>";
		return $xmlData;
	}
	
	
?>