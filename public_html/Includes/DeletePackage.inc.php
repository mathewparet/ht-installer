<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		LogMessage("Tried to access DeletePackage.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load DeletePackage.inc.php without through index.php");
	}

	function DeletePackageAction()
	{
		global $_GET;
		$xmlData.=DeletePackage($_GET['Package']);
		$xmlData.=UpdatePackage();
		return $xmlData;
	}
	
	function DeleteCheckedPackagesAction()
	{
		global $_GET;
		$xmlData='';
		foreach($_GET['Package'] as $Pack)
		{
			$xmlData.=DeletePackage($Pack);
		}
		$xmlData.=UpdatePackage();
		return $xmlData;
	}

	function DeletePackage($PName)
	{
		if(@unlink(DPX_HT_INSTALLER_PATH."/packages/".$PName))
		{
			LogMessage("Package <b>".$PName."</b> was deleted");
			return "<message type='info'><![CDATA[Package <b>".$PName."</b> was deleted]]></message>\n";
		}
		else
		{
			LogMessage("Package ".$PName." could not be deleted.","error");
			return "<message type='error'><![CDATA[Package <b>".$PName."</b> could not be deleted.]]></message>\n";
		}
	}


?>