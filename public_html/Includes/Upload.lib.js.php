<?php Header("Content-Type: application/x-javascript; charset=UTF-8"); ?>
<?php define('DPX_HT_INSTALLER_LOADED',true); ?>
<?php include_once 'GetMaxSize.inc.php'; ?>
var UploadPackage_sid=CreateStatus();
var UploadFile=new Array();

function UploadPackage()
{
	var FilterString='';
	for(i=0;i<SupportedFormats.length;i++)
	{
		if(FilterString.length!=0)
			FilterString=FilterString+";";
		FilterString=FilterString+"*."+SupportedFormats[i];
	}
	fc.callFunction("_root",'ChooseFile',['Packages',FilterString]);
}

function ShowError(msg)
{
	alert("Cannot upload: "+msg);
}
			
function SetFileName(fname)
{
	var details=new Array();
	details=fname.split(",");
	var maxsize=<?php print GetMaxSize(); ?>;
	if(details[1]>maxsize)
	{
		alert("File too big to upload!. PHP settings prevent you from uploading files greater than "+PerfectSize(maxsize)+". Please contact server administrator for more details.");
		return;
	}
	UploadFile=fname;
	fc.callFunction("_root",'StartUpload',"packages/");
}

function SetProgress(perc)
{
	fdetails=UploadFile.split(",");
	SetStatus("Uploading ("+fdetails[0]+") <i>"+Math.round(perc*100)+"%</i>",UploadPackage_sid);
}

function UploadComplete()
{
	UpdatePackageList();
	SetStatus('',UploadPackage_sid);
}
