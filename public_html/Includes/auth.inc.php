<?php

	if(!defined("DPX_HT_INSTALLER_LOADED"))
	{
		include "LogMessage.inc.php";
		LogMessage("Tried to access auth.inc.php by hack, i.e. not through index.php. This could be a possible hack attempt.","alert");
		die("Attack Detected; cannot load auth.inc.php without through index.php");
	}

	if($_GET['Action']=="Logout")
	{
		global $_SESSION;
		unset($_SESSION['UserName']);
		unset($_SESSION['Password']);
		unset($_SESSION['Stage']);
		$Info[]="You have successfully logged out.";
		LogMessage("User logged out");
		session_destroy();
	}
	if(file_exists("config.php"))
	{
		global $Config,$_POST,$_SESSION,$_SERVER;
		$CONFIG_STRING=ioncube_read_file("config.php",$we,$_SERVER['HTTP_HOST']);
		parse_str($CONFIG_STRING,$Config);
		
		if($_POST['Action']=="Login")
		{
			if($_POST['UserName']!=$Config['UserName'])
			{
				$Err[]="Invalid UserName!";
				$State['UserName']="error";
			}
			if($_POST['Password']!=$Config['Password'])
			{
				$Err[]="Invalid Password!";
				$State['Password']="error";
			}
			if(!$Err)
			{
				$_SESSION['UserName']=$_POST['UserName'];
				$_SESSION['Password']=$_POST['Password'];
				$_SESSION['Stage']=4;
				LogMessage("User logged in");
				header(sprintf("Location: %s",$HTTP_SERVER_VARS['PHP_SELF']));
				exit;
			}
			else
			{
				LogMessage("User login failed. Wrong account info was provided","error");
				$Err[]="Could not login";
			}
		}
		if($_SESSION['UserName']!=$Config['UserName'] || $_SESSION['Password']!=$Config['Password'])
		{
			if($_SERVER['QUERY_STRING']!='' && $_GET['Action']!="Logout")
			{
				$Err[]="Access Deined!";
				LogMessage("Access denied to user!","error");
			}
			$_SESSION['Stage']=3;
		}
	}
	else
	{
		if(file_exists("settings.php"))
		{
//			include_once "settings.php";
			$_SecurityCode=ioncube_read_file("settings.php");
			$SecurityCode=substr($_SecurityCode,strpos($_SecurityCode,"//")+2);
			$SecurityCode=substr($SecurityCode,0,strpos($SecurityCode,"\\"));
			if(isset($SecurityCode) && $SecurityCode!='')
			{
				if($_POST['Action']=="Unlock")
				{
					if($_POST['SecurityCode']!=$SecurityCode)
					{
						$Err[]="Wrong Security Code";
						$State['SecurityCode']="error";
						LogMessage("Wrong Security Code was provided!","error");
						$Err[]="Couldn't unlock HT Installer";
						$_SESSION['Stage']=1;
					}
					else
					{
						$_SESSION['Stage']=2;
						$_SESSION['SecurityCode']=$_POST['SecurityCode'];
						LogMessage("Security code check passed");
					}
				}
				if($_SESSION['SecurityCode']!=$SecurityCode)
				{
					$_SESSION['Stage']=1;
				}
				else
				{
					$_SESSION['Stage']=2;
				}
			}
		}
		else
		{
			// no settings file
			$_SESSION['Stage']=2;
		}
		if($_POST['Action']=="Create")
		{
			//	create config file
			if(strlen($_POST['UserName'])<5)
			{
				$Err[]="UserName should have atleast 5 characters";
				$State['UserName']="error";
			}
			if(strlen($_POST['Password'])<5)
			{
				$Err[]="Password should have atleast 5 characters";
				$State['Password']="error";
			}
			if($_POST['Password']!=$_POST['RePassword'])
			{
				$Err[]="Passwords do not match";
				$State['Password']="error";
				$State['RePassword']="error";
			}
			if(!$Err)
			{
				$_config_data="UserName=".$_POST['UserName']."&Password=".$_POST['Password']."&ChunkSize=1048576&PageSize=10&Debug=0";
				ioncube_write_file("config.php",$_config_data,true,$_SERVER['HTTP_HOST']);
				LogMessage("User account <i>".$_POST['UserName']."</i> successfully created!");
				$Info[]="User account <i>".$_POST['UserName']."</i> successfully created!";
				LogMessage("Configuration file successfully created!");
				$Info[]="Configuration file successfully created!";
				$_SESSION['Stage']=3;
			}
			else
			{
				LogMessage("Could not create user account!",'error');
				$Err[]="Could not create user account!";
			}
		}
	}
?>