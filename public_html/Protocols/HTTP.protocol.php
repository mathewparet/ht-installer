<?php

	function ParseHTTPHeaders($Header)
	{
		global $Config;
		$Heads=$Rvalue=array();
		$Heads=explode("\r\n",$Header);
		foreach($Heads as $Head)
		{
			$Key=substr($Head,0,strpos($Head,": "));
			if($Key=='')
			{
				$Value=substr($Head,strlen($Key));
				$Key="First";
			}
			else
				$Value=substr($Head,strlen($Key)+strlen(": "));
			$Rvalue[$Key]=$Value;
		}
		if(isset($Rvalue['Content-Range']))
		{
			$value=$Rvalue['Content-Range'];
			$temp=substr($value,strlen("bytes "));
			$Rvalue['HTIStart']=substr($temp,0,strpos($temp,'-'));
			$temp=substr($value,strpos($value,"-")+1);
			$Rvalue['HTIEnd']=substr($temp,0,strpos($temp,"/"));
			$Rvalue['HTISize']=substr($value,strpos($value,"/")+1);
			$Rvalue['HTIResume']="true";
		}
		else
		{
			$Rvalue['HTIResume']="false";
		}
		if($Config['Debug'] && $Rvalue['HTIStart']=='0')
		{
			$tempHeader=str_replace("\r\n","<br>",$Header);
			LogMessage($tempHeader,"DEBUG");
		}		
		if(isset($Rvalue['Content-Disposition']))
		{
			$value=$Rvalue['Content-Disposition'];
			if(substr($value,strpos($value,"filename=")+strlen("filename="),1)=="\"")
			{
				$temp=substr($value,strpos($value,"filename=")+strlen("filename=")+1);
				$Rvalue['HTIFileName']=substr($temp,0,strlen($temp)-1);
			}
			else
			{
				$temp=substr($value,strpos($value,"filename=")+strlen("filename="));
				$Rvalue['HTIFileName']=substr($temp,0,strlen($temp));
			}
		}
		return $Rvalue;
	}
	
	function HTTPSFsockFileDownload($File,$Chunk)
	{
		return HTTPFsockFileDownload($File,$Chunk);
	}
	
	function HTTPFsockFileDownload($File,$Chunk)
	{
		global $_SESSION,$SupportedArchives,$Config;
		$url=parse_url($File);
		if($url['port']=='') $url['port']=80;
		$fp=@fsockopen($url['host'],$url['port']);
		if(!$fp)
		{
			return LogMessage("Could not connect to <b>(".$url['host'].")</b>!",'error');
		}
		$HeaderMessage="GET ".$url['path'];
		if(strpos($File,"?")!==false)
			$HeaderMessage.="?".$url['query'];
		if(isset($_SESSION['FileSize']))
		{
			if(($_SESSION['FilePos']+$Chunk) > $_SESSION['FileSize'])
				$value=$_SESSION['FileSize'];
			else
				$value=$_SESSION['FilePos']+$Chunk;
		}
		else
		{
			$value=$_SESSION['FilePos']+$Chunk;
		}
		
		if(isset($_SESSION['HTIResume']) && $_SESSION['HTIResume']=="false")
		{
			// no resume
			$HeaderMessage.=" HTTP/1.0\r\nHost: "/$url['host']."\r\n\r\n";
		}
		else
		{
			if($_SESSION['FilePos']!=0)	$_SESSION['FilePos']=$_SESSION['FilePos']+1;
			$HeaderMessage.=" HTTP/1.0\r\nHost: ".$url['host']."\r\nRange: bytes=".$_SESSION['FilePos']."-";
			$HeaderMessage.=$value;
			$HeaderMessage.="\r\n\r\n";
		}
		
		if(!@fputs($fp,$HeaderMessage))
		{
			return LogMessage("Couldn't communicate with server!",'error');
		}
		while(!feof($fp))
		{
			$data.=@fgets($fp,128);
		}
		$_SESSION['FilePos']=$_SESSION['FilePos']+strlen($data);
		fclose($fp);
		$rets=ParseHTTPHeaders(substr($data,0,strpos($data,"\r\n\r\n")));
		$_SESSION['HTIResume']=$rets['HTIResume'];
		$FileName=basename($File);
		
		if(isset($rets['HTIFileName']))
			$FileName=$rets['HTIFileName'];
			
		if(file_exists(DPX_HT_INSTALLER_PATH."/packages/".$FileName))
		{
			$FileName=AltFile($FileName,DPX_HT_INSTALLER_PATH."/packages/");
		}
		if(isset($_SESSION['FileRName']))
			$FileName=$_SESSION['FileRName'];
	
		if($_SESSION['HTIResume']=='true')
		{
			$_SESSION['FileName']=$FileName;
		}
		else
		{
			if($_SESSION['FileStatus']!="Started")
				$_SESSION['FileName']=$FileName;
		}
		
		if(!in_array(strtolower(FileExt($_SESSION['FileName'])),$SupportedArchives))
			return LogMessage("Unsupported package type <b>(".strtolower(FileExt($_SESSION['FileName'])).")</b>!!",'error');
			
		if(!@file_exists(DPX_HT_INSTALLER_PATH."/_temp/".$_SESSION['FileName']) && $Config['Debug'])
		{
			foreach($rets as $k=>$v)
			{
				LogMessage($k." = ".$v,"DEBUG");
			}
		}
		if(strpos($rets['First'],"206")===false && strpos($rets['First'],"200")===false)
		{
			$retmessage="Server returned: <b>".substr($rets['First'],strpos($rets['First']," ")+1)." (";
			$temp=substr($rets['First'],strpos($rets['First']," ")+1);
			if(strpos($temp," ")===false)
				$hstatus=$temp;
			else
				$hstatus=substr($temp,0,strpos($temp," "));
			switch($hstatus)
			{
				case 404:	$retmessage.="File Not Found"; break;
				case 301:	$retmessage.="Moved Permanently to ".$rets['Location']; break;
				case 302:
				case 307:
				case 303:	$retmessage.="Moved temporarity to ".$rets['Location']; break;
				case 305:	$retmessage.="Use Proxy"; break;
				case 401:	$retmessage.="Unauthorized"; break;
				case 403:	$retmessage.="Forbidden"; break;
				case 408:	$retmessage.="Request Timeout"; break;
				case 410:	$retmessage.="Gone"; break;
				case 416:	$retmessage.="Requested Range Not Satisfiable"; break;
				case 500:	$retmessage.="Internal Server Error"; break;
				case 502:	$retmessage.="Bad Gateway"; break;
				case 503:	$retmessage.="Service Unavailable"; break;
				case 504:	$retmessage.="Gateway Timeout"; break;
				case 505:	$retmessage.="HTTP Version Not Supported"; break;
			}
			$retmessage.=")</b> for the URL <b>(".$File.")</b>.";
			return LogMessage($retmessage,'error');
		}
		if($_SESSION['HTIResume']=="true")
		{
			$localfile=fopen(DPX_HT_INSTALLER_PATH."/_temp/".$FileName,"ab");
			$_SESSION['FilePos']=$rets['HTIEnd'];
			$_SESSION['FileSize']=$rets['HTISize'];
		}
		else
		{
			$localfile=fopen(DPX_HT_INSTALLER_PATH."/_temp/".$FileName,"wb");
		}
		
		if($_SESSION['HTIResume']=="true")
		{
			if($_SESSION['FileSize']<=($_SESSION['FilePos']+1))
			{
				$_SESSION['FileStatus']="Completed";
				LogMessage("File (<b>".$_SESSION['URL']."</b>) downloaded to server as <b>".$FileName."</b>");
			}
			else
			{
				$_SESSION['FileStatus']='Started';
			}
			$_SESSION['FileRName']=$FileName;
		}
		else
		{
			if($_SESSION['FileStatus']=='Started')
			{
				$_SESSION['FileStatus']="Completed";
			}
			else
			{
				$_SESSION['FileStatus']='Started';
				$_SESSION['FileRName']=$FileName;
			}
		}
		if($_SESSION['HTIResume']=="true")
			$contents=substr($data,strpos($data,"\r\n\r\n")+strlen("\r\n\r\n"));
		else
			$contents=$data;
		@fwrite($localfile,$contents);
		@fclose($localfile);
		return "";
	}


?>