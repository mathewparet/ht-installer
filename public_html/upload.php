<?php
	define(DPX_HT_INSTALLER_PATH,dirname(__FILE__));
	define(DPX_HT_INSTALLER_LOADED,true);
	include_once 'Includes/LogMessage.inc.php';
	include_once 'Includes/FileExt.inc.php';
	include_once 'Includes/AltFile.inc.php';
	if ($_FILES['Filedata']['name']) 
	{
		$uploadDir = DPX_HT_INSTALLER_PATH."/".$HTTP_GET_VARS['Destination'];	//	end with spash /
		$uFile=basename($_FILES['Filedata']['name']);
		if(file_exists($uploadDir.$uFile))
		{
			$uFile=AltFile($uFile,$uploadDir);
		}
		$uploadFile = $uploadDir.$uFile;
		LogMessage("File Uploaded <b>(".basename($uploadFile).")</b>");
		move_uploaded_file($_FILES['Filedata']['tmp_name'], $uploadFile);
	}
?>
