<?php

	global $SupportedArchives;
	$SupportedArchives[]="zip";

	function ZIPArchiveContents($file)
	{
		$fp = fopen($file, 'rb');
		while (true)
		{
			$f++;
			$data = fread($fp, 30);
			$sig  = substr($data, 0, 4);
			if ($sig != "\x50\x4b\x03\x04") {
				break;
			}
			$unpack = unpack("vversion/vflag/vcompression/vmtime/vmdate/Vcrc/Vcompressed_size/Vsize/vfilename_len/vextra_len", substr($data, 4));
			$files[$f]['size']    = $unpack['compressed_size'];
			$files[$f]['fn_size'] = $unpack['filename_len'];
			$files[$f]['crc32']	  = $unpack['crc'];
			$files[$f]['compression'] = $unpack['compression'];
			$files[$f]['name']    = fread($fp, $files[$f]['fn_size']);
			if($files[$f]['size']>0)
				$files[$f]['data']    = fread($fp, $files[$f]['size']);
			if (!empty($files[$f]['data']) && $files[$f]['compression'] == 8) {
				$files[$f]['data'] = @gzinflate( $files[$f]['data'] );
			} else if(!empty($files[$f]['data']) && $files[$f]['compression'] == 12) {
				$files[$f]['data'] = @bzdecompress( $files[$f]['data'] );
			}
			
		}
		fclose($fp);
		return $files;
	}
	
	function ZIPArchiveExtract( $file, $ex_dir = null,$SubPath='' )
	{
		if ($ex_dir != null AND !is_dir($ex_dir)) 
		{
			return false;
		}
		$ex_dir = str_replace( '\\\\', '/', $ex_dir );
		if (substr($ex_dir, -1) != "/") 
		{
			$ex_dir .= '/';
		}
		$fp = fopen($file, 'rb');
		while (true)
		{
			$f++;
			$data = fread($fp, 30);
			$sig  = substr($data, 0, 4);
			if ($sig != "\x50\x4b\x03\x04") 
			{
				break;
			}
			$unpack = unpack("vversion/vflag/vcompression/vmtime/vmdate/Vcrc/Vcompressed_size/Vsize/vfilename_len/vextra_len", substr($data, 4));
			$files[$f]['size']    = $unpack['compressed_size'];
			$files[$f]['fn_size'] = $unpack['filename_len'];
			$files[$f]['crc32']	  = $unpack['crc'];
			$files[$f]['compression'] = $unpack['compression'];
			$files[$f]['name']    = fread($fp, $files[$f]['fn_size']);
			if($files[$f]['size']>0)
				$files[$f]['data']    = fread($fp, $files[$f]['size']);
			if (!empty($files[$f]['data']) && $files[$f]['compression'] == 8) {
				$files[$f]['data'] = @gzinflate( $files[$f]['data'] );
			} else if(!empty($files[$f]['data']) && $files[$f]['compression'] == 12) {
				$files[$f]['data'] = @bzdecompress( $files[$f]['data'] );
			}
		}
		fclose($fp);
		foreach ($files as $file) 
		{
			$file_name = basename($file['name']);   // get the file name
			$dir_name  = dirname($file['name']);    // get the directory name
			if (!$file_name OR !$file['data']) 
			{
				continue;
			}
			if($SubPath!='')
			{
				if(@strpos($dir_name,$SubPath)===false)
				{
						continue;
				}
				else
				{
					$dir_name=substr($dir_name,strlen($SubPath)+1);
				}
			}
			$last = "";
			$c_dir = explode( "/", $dir_name );
			foreach($c_dir as $dir) 
			{
				if(!is_dir($ex_dir. $last .$dir)) 
				{
					mkdir( $ex_dir. $last .$dir, 0777 );
				}
				$last .= $dir . '/';
			}
			if (!($fp = fopen($ex_dir . $dir_name .'/'. $file_name, 'wb'))) 
			{
				return false;
			}
			fwrite($fp, $file['data']);
			fclose($fp);
		}
		return true;
	}

	
?>