<?php

	global $SupportedArchives;
	$SupportedArchives[]="tar";
	$SupportedArchives[]="gz";
	$SupportedArchives[]="tgz";
	
	function TGZArchiveExtract($file,$ex_dir=null,$SubPath='')
	{
		return GZArchiveExtract($file,$ex_dir,$SubPath);
	}
	
	function TGZArchiveContents($file)
	{
		return GZArchiveContents($file);
	}

	function _GZArchiveTemp($file)
	{
		$zd=gzopen($file,"r");
		$contents=gzread($zd,filesize($file));
		gzclose($zd);
		if(!$contents)
			return false;
		$tmpFile=DPX_HT_INSTALLER_PATH."/_temp/".basename($file).".tmp";
		if(file_put_contents($tmpFile,$contents))
			return $tmpFile;
		return false;
	}
	
	function GZArchiveExtract( $file, $ex_dir = null, $SubPath='' )
	{
		$tmpFile=_GZArchiveTemp($file);
		if(!$tmpFile)
			return false;
		if(!TARArchiveExtract($tmpFile,$ex_dir,$SubPath))
		{
			unlink($tmpFile);
			return false;
		}
		unlink($tmpFile);
		return true;
	}
	
	function GZArchiveContents($file)
	{
		$tmpFile=_GZArchiveTemp($file);
		if(!$tmpFile)
			return false;
		$files=TARArchiveContents($tmpFile);
		unlink($tmpFile);
		return $files;
	}

	function TARArchiveContents($file)
	{
		$fp = fopen($file, 'rb');
		while (!feof($fp))
		{
			$f++;
			$data = fread($fp, 512);
			$checksum = 0;
			for ($i = 0; $i < 148; $i++)
			{
				$checksum += ord(substr($buffer, $i, 1));
			}
			for ($i = 148; $i < 156; $i++)
			{
				$checksum += ord(' ');
			}
			for ($i = 156, $j = 0; $i < 512; $i++, $j++)
			{
				$checksum += ord(substr($buffer, $j, 1));
			}
			$unpack = unpack("a100filename/a8mode/a8uid/a8gid/a12size/a12mtime/a8checksum/a1typeflag/a100link/a6magic/a2version/a32uname/a32gname/a8devmajor/a8devminor", $data);
			$files[$f]['checksum'] = octdec(trim($unpack['checksum']));
			if ($checksum == 256 && $files[$f]['checksum'] == 0) 
			{
				break;
			}
			$unpack['typeflag'] = trim($unpack['typeflag']);
			if ($unpack['typeflag'] != "0") 
			{
				continue;
			}
			$files[$f]['name'] = trim($unpack['filename']);
			$files[$f]['size'] = octdec(trim($unpack['size']));
			$files[$f]['data'] = fread($fp, $files[$f]['size']);
			if (strlen($files[$f]['data']) != $files[$f]['size']) 
			{
				return false;
			}
			fread($fp, 512 - ($files[$f]['size'] % 512));
		}
		fclose($fp);
		return $files;
	}
	
	
	function TARArchiveExtract( $file, $ex_dir = null, $SubPath='' )
	{
		if ($ex_dir != null AND !is_dir($ex_dir)) 
		{
			return false;
		}
		$ex_dir = str_replace( '\\', '/', $ex_dir );
		if (substr($ex_dir, -1) != "/") {
			$ex_dir .= '/';
		}
		$fp = fopen($file, 'rb');
		while (!feof($fp))
		{
			$f++;
			$data = fread($fp, 512);
			$checksum = 0;
			for ($i = 0; $i < 148; $i++)
			{
				$checksum += ord(substr($buffer, $i, 1));
			}
			
			for ($i = 148; $i < 156; $i++)
			{
				$checksum += ord(' ');
			}
			for ($i = 156, $j = 0; $i < 512; $i++, $j++)
			{
				$checksum += ord(substr($buffer, $j, 1));
			}
			$unpack = unpack("a100filename/a8mode/a8uid/a8gid/a12size/a12mtime/a8checksum/a1typeflag/a100link/a6magic/a2version/a32uname/a32gname/a8devmajor/a8devminor", $data);
			$files[$f]['checksum'] = octdec(trim($unpack['checksum']));
			if ($checksum == 256 && $files[$f]['checksum'] == 0) 
			{
				break;
			}
			$unpack['typeflag'] = trim($unpack['typeflag']);
			if ($unpack['typeflag'] != "0") 
			{
				continue;
			}
			$files[$f]['name'] = trim($unpack['filename']);
			$files[$f]['size'] = octdec(trim($unpack['size']));
			$files[$f]['data'] = fread($fp, $files[$f]['size']);
			if (strlen($files[$f]['data']) != $files[$f]['size']) 
			{
				return false;
			}
			fread($fp, 512 - ($files[$f]['size'] % 512));
		}
		fclose($fp);
		foreach ($files as $file) 
		{
			$file_name = basename($file['name']);   // get the file name
			$dir_name  = dirname($file['name']);    // get the directory name
			if (!$file_name OR !$file['data']) 
			{
				continue;
			}
			if($SubPath!='')
			{
				if(strpos($dir_name,$SubPath)===false)
				{
						continue;
				}
				else
				{
					$dir_name=substr($dir_name,strlen($SubPath)+1);
				}
			}
			$last = "";
			$c_dir = explode( "/", $dir_name );
			foreach($c_dir as $dir) 
			{
				if(!is_dir($ex_dir. $last .$dir)) 
				{
					mkdir( $ex_dir. $last .$dir, 0777 );
				}
				$last .= $dir . '/';
			}
			if (!($fp = fopen($ex_dir . $dir_name .'/'. $file_name, 'wb'))) 
			{
				return false;
			}
			fwrite($fp, $file['data']);
			fclose($fp);
		}
		return true;
	}


?>