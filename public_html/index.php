<?php
	session_start();
	
	define("DPX_HT_INSTALLER_LOADED",true);
	
	global $errXML,$Config;

	$State=array();

	$Err=array();

	$Info=array();
	
	$SupportedArchives=array();
	
	define("DPX_HT_INSTALLER_PATH",dirname(__FILE__));
		
	include_once dirname(__FILE__)."/Includes/LogMessage.inc.php";
	include_once dirname(__FILE__)."/Includes/makefilelist.inc.php";
	
	// load package formats

	$FormatDefinitions=MakeFileList(dirname(__FILE__)."/Formats/",array(".format.php"));

	foreach($FormatDefinitions as $FormatDefinition)
	{
		include_once $FormatDefinition;
	}
	
	// load protocol definitions
	
	$ProtocolDefinitions=MakeFileList(dirname(__FILE__)."/Protocols/",array(".protocol.php"));
	
	foreach($ProtocolDefinitions as $ProtocolDefinition)
	{
		include_once $ProtocolDefinition;
	}
	
	// load includes
	
	$IncFiles=MakeFileList(dirname(__FILE__)."/Includes/",array(".inc.php"));
	
	foreach($IncFiles as $IncFile)
	{
		if(strpos(strtolower($IncFile),"makefilelist.inc.php")===false && strpos(strtolower($IncFile),"logmessage.inc.php")===false)
		{
			include_once $IncFile;
		}
	}

	define("HTI_VERSION","1.2");
	
	if($_SESSION['Stage']==4)
	{
		$xmlData.="<?xml version='1.0'?>\n";
		$xmlData.="\t<root>\n";
		if(isset($_GET['Action']))
		{
			$ActionHandler=$_GET['Action']."Action";
		}
		elseif(basename($_SERVER['REQUEST_URI'])!=basename($_SERVER['SCRIPT_FILENAME']))
		{
			$_Request=substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],"index.php/")+strlen("index.php/"));
			$Request=explode("/",$_Request);
			$ActionHandler=$Request[0]."Action";
		}
		
		if(strlen($ActionHandler)>strlen("Action"))
		{
			if(function_exists($ActionHandler))
			{
				header("Content-type: text/xml");
				$xmlData.=$ActionHandler();
				$xmlData.=$errXML."\t</root>";
				print $xmlData;
				die();
			}
			else
			{
				header("Content-type: text/xml");
				$xmlData.=LogMessage("Operation undefined. Please contact vendor!!","error");
				$xmlData.="</root>";
				print $xmlData;
				die();
			}
		}
	}

?>
<?php
	if($HTTP_SESSION_VARS['Stage']==1) $htiTitle="Security Lock";
	if($HTTP_SESSION_VARS['Stage']==2) $htiTitle="Admin Account";
	if($HTTP_SESSION_VARS['Stage']==3) $htiTitle="Login";
	if($HTTP_SESSION_VARS['Stage']==4) $htiTitle="Welcome";
?>
<html>
<head>
<title><?php print $htiTitle; ?> / HT Installer v<?php print HTI_VERSION; ?></title>
<link href="style.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="Includes/Ajax.lib.js"></script>
	<script language="javascript" src="Includes/SetStatus.func.js"></script>
	<script language="javascript">
		<!--
			var SupportedFormats=new Array();
			<?php
				foreach($SupportedArchives as $SupportedArchive)
				{
			?>
			SupportedFormats.push('<?php print $SupportedArchive; ?>');
			<?php
				}
			?>
			var StatusInfo=new Array();
		-->
	</script>
	<?php if($_SESSION['Stage']==4) {?>
	<?php
		$JSFiles=MakeFileList(dirname(__FILE__)."/Includes/",array(".js"));
		foreach($JSFiles as $JSFile)
		{
			if(!in_array(array('Ajax.lib.js','SetStatus.func,js'),array(basename($JSFile))))
			{
	?>
				<script language="javascript" type="text/javascript" src="Includes/<?php print basename($JSFile); ?>"></script>
	<?php
			}
		}
	?>
	<script language="javascript">
<!--
<!--
<!--
		//start
		var AjaxUpdatePackage=new Ajax("index.php","GET");
		var UpdatePackageList_sid=CreateStatus();
		AjaxUpdatePackage.onResponse=function(r)
		{
			if(r.State==4)
			{
				SetStatus('',UpdatePackageList_sid);
				var xmldoc=r.XML;
				ShowMessages(xmldoc);
				DisplayPackages(xmldoc);
				var root=xmldoc.getElementsByTagName('var');
				if(root.length)
					document.getElementById("TotalPackageSize").innerHTML=root[0].getAttribute("value");
			}
		}
		
		AjaxUpdatePackage.onError=function(er)
		{
			alert("Communication error on Update Package: "+er);
			SetStatus('',UpdatePackageList_sid);
		}
		
		function addRow(name,size,packdate,count)
		{
			var tblPL=document.getElementById("divPackageList");
			tblPL.innerHTML=tblPL.innerHTML+"<tr id='ListItem'><td width='20' height='20' valign='top'><input name='Check' value='"+name+"' type='checkbox' id='Check"+count+"' onClick='javascript: CheckClick(this);'></td><td width='100%' valign='top'>"+name+"</td>\t<td width='74' valign='top'>"+size+"</td><td width='244' valign='top'><div align='right'>"+packdate+"</div></td><td width='172' valign='top'><div align='center'><a href='<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>/download/"+name+"' title='Download "+name+"'><img src='download.png' border='0'></a> / <span class='Install'><a href='#' onClick='javascript: InstallClick(\""+name+"\"); return false;' title='Install "+name+"'>Install</a></span> / <span class='Delete'><a href='#' onClick='javascript: Delete(\""+name+"\"); return false;' title='Delete "+name+"'>Delete</a></span> </div></td></tr>";
			var selText=document.getElementById("InstallPackageList").innerHTML;
			selText=selText+"<option value='"+name+"'>"+name+" ("+size+")</option>";
			document.getElementById("InstallPackageList").innerHTML=selText;
		}
		
		function UpdatePackageList()
		{
			SetStatus("Refreshing View",UpdatePackageList_sid);
			AjaxUpdatePackage.Go("Action=UpdatePackageList");
		}
		
		function DisplayPackages(xmldoc)
		{
				var root=xmldoc.getElementsByTagName("package");					
				document.getElementById("divPackageList").innerHTML="";
				var prevData=document.getElementById("InstallPackageList").value;
				document.getElementById("InstallPackageList").innerHTML="<option>===| Select Package |===</option>";
				for(i=0; i<root.length; i++)
				{
					addRow(root[i].getAttribute('name'),root[i].getAttribute('size'),root[i].getAttribute('date'),i+1);
				}
				if(!root.length)
					document.getElementById("divPackageList").innerHTML="<tr width='100%'><td><center>No Packages Availabale yet!</center></td></tr>";
				else
					document.getElementById("divPackageList").innerHTML=document.getElementById("divPackageList").innerHTML+"<tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td><img src='spacer.gif' alt='' width='20' height='1'></td><td></td><td><img src='spacer.gif' alt='' width='74' height='1'></td><td><img src='spacer.gif' alt='' width='244' height='1'></td><td><img src='spacer.gif' alt='' width='172' height='1'></td></tr>";	
				document.getElementById("CheckAll").checked=false;
				document.getElementById("InstallPackageList").value=prevData;
		}
		//stop
			function CheckClick()
			{
				document.getElementById("CheckAll").checked=false;
			}
			
			function CheckAllClick()
			{
				i=0;
				i++;
				while(ptr=document.getElementById("Check"+i))
				{
					if(ptr=="undefined")	break;
					if(document.getElementById("CheckAll").checked)
					{
						ptr.checked=true;
						document.getElementById("CheckAll").checked=true;
					}
					else
					{
						ptr.checked=false;
						document.getElementById("CheckAll").checked=false;
					}
					i++;
				}
			}
			var myTab=new Tab('MenuBlock','SelectedMenu','Menu','DescData','myTab');
			myTab.Add("BackupPackages","Backup Packages","Following is the list of backup packages available on this server (<?php print $_SERVER['SERVER_NAME']; ?>):","BackupPackagesID");
			myTab.Add("InstallPackage","Install Package","Install a package now.","InstallPackageID");
			myTab.Add("Settings","Settings","Configure HT Installer","SettingsID");
			myTab.Add("Log","View Log","Log entries. <a href='#' onClick='javascript: LoadLog(0); return false;'>&lt; Newer</a> - <a href='#' onClick='javascript: LoadLog(0); return false;'>Older &gt;</a> - <a href='#' onClick='javascript: CrearLog(); return false;'>Clear Log</a>","LogID");
			myTab.Add("AccountSettings","Account Settings","You may change your username & password here.","AccountSettingsID");
			myTab.Add("AboutHTInstaller","About HT Installer","Some information about HT Installer.","AboutHTInstallerID");
			myTab.Add("StatusPage","Status","Cusrrent status of operation in HT Installer.","StatusPageID");
			myTab.onClick=function(TName)
			{
				return 1;
			}
			myTab.onShow=function(TName)
			{
				if(TName=="Log")
				{
					LoadLog(0);
				}
				if(TName=='AboutHTInstaller')
				{
					document.getElementById("LogFileSize").innerHTML=document.getElementById("LogFileSize").innerHTML+', Updating information...';
					document.getElementById("TotalPackageSize").innerHTML=document.getElementById("TotalPackageSize").innerHTML+', Updating information...';
					AjaxLogView.Go("Action=LogFileSize");
					AjaxUpdatePackage.Go("Action=TotalPackageSize");
				}
				if(TName=="Settings")
				{
					LoadSettings();
				}
			}
			var fc=new JSFCommunicator();
			function PreloadImages() 
			{ //v3.0
			  	var d=document; 
			  	if(d.images)
			  	{ 
			  		if(!d.MM_p)
						d.MM_p=new Array();
					var i,j=d.MM_p.length,a=PreloadImages.arguments;
					for(i=0; i<a.length; i++)
						if (a[i].indexOf("#")!=0)
						{ 
							d.MM_p[j]=new Image; 
							d.MM_p[j++].src=a[i];
						}
				}
			}

			window.onload=function()
			{
				PreloadImages('download.png','progress.gif','spacer.gif')
				UpdatePackageList();
				var flashMovie=document.getElementById("uploadSWF");
				fc.setMovie(flashMovie);
				myTab.Install("BackupPackages");
			}

			function FindObj(theObj, theDoc)
			{
				var p, i, foundObj;
				if(!theDoc) 
					theDoc = document;
				if( (p = theObj.indexOf("?")) > 0 && parent.frames.length)
				{
					theDoc = parent.frames[theObj.substring(p+1)].document;
					theObj = theObj.substring(0,p);
				}
				if(!(foundObj = theDoc[theObj]) && theDoc.all) 
					foundObj = theDoc.all[theObj];
				for (i=0; !foundObj && i < theDoc.forms.length; i++) 
					foundObj = theDoc.forms[i][theObj];
				for(i=0; !foundObj && theDoc.layers && i < theDoc.layers.length; i++) 
					foundObj = findObj(theObj,theDoc.layers[i].document);
				if(!foundObj && document.getElementById) 
					foundObj = document.getElementById(theObj);
				return foundObj;
			}


		// Example: showHideLayers(Layer1,'','show',Layer2,'','hide');
		
		function ShowHideLayers()
		{ 
			var i, visStr, obj, args = ShowHideLayers.arguments;
			for (i=0; i<(args.length-2); i+=3)
			{
				if ((obj = FindObj(args[i])) != null)
				{
					visStr = args[i+2];
					if (obj.style)
					{
						obj = obj.style;
						if(visStr == 'show') 
							visStr = 'visible';
						else if(visStr == 'hide') 
							visStr = 'hidden';
					}
					obj.visibility = visStr;
				}
			}
		}


//-->
</script>
	<?php } ?>
    <style type="text/css">
<!--
#LogDetails {
	position:absolute;
	left:223px;
	top:119px;
	width:630px;
	height:211px;
	z-index:2;
	background-color: #000000;
	overflow:auto;
}
-->
    </style>
</head>
<body>
<div id="SubFileListBox">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666666">
    <tr>
      <td><div align="center"><strong><font color="#FFFFFF">Package Contents</font></strong></div></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  	<tr>
		<td align="right"><a href="#" onClick="javascript: document.getElementById('SubFileList').innerHTML=''; document.getElementById('SubFileListBox').style.display='none'; return false;">X</a>&nbsp;</td>
	</tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" id="SubFileList">
  </table>
</div>
<div id="LogDetails" style="visibility:hidden">
              <table width="629" border="0">
                <tr>
                  <td width="73">Date / Time </td>
                  <td width="414"><span id="LogDate">Date</span></td>
                </tr>
                <tr>
                  <td>Type</td>
                  <td><span id="LogType">Type</span></td>
                </tr>
                <tr>
                  <td align="left" valign="top">Message</td>
                  <td align="left" valign="top"><span id="LogMessage">Message</span></td>
                </tr>
                <tr>
                  <td>IP</td>
                  <td><span id="LogIP">IP</span></td>
                </tr>
                <tr>
                  <td>Referer</td>
                  <td><span id="LogReferer">Referer</span></td>
                </tr>
                <tr>
                  <td>Query String </td>
                  <td><span id="LogQueryString">Query String</span></td>
                </tr>
                <tr>
                  <td colspan="2"><div align="center" class="footer"><a href="#" onClick="javascript: ShowHideLayers('LogDetails','','hide'); return false;">[ Close ]</a></div></td>
                </tr>
              </table>
            </div>
<table id="main" width="100%" border="0" cellpadding="0" cellspacing="0"><!--DWLayoutTable-->
  <tr>
    <td width="100%">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" id="main">
        <!--DWLayoutTable-->
        <tr>
          <td width="100%" valign="top"><h1>HT Installer v<?php print HTI_VERSION; ?> - [<?php print $htiTitle; ?><?php if($HTTP_SESSION_VARS['UserName']!='') { ?>, <?php print $HTTP_SESSION_VARS['UserName']; ?><?php } ?>]<?php if($HTTP_SESSION_VARS['UserName']!='') { ?> - <a href="<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>?Action=Logout">Logout</a><?php } ?></h1></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
          <td width="100%" valign="top" bgcolor="#6699FF">
		  <div id="msg">
		  <?php if($Err) { ?>
		  	<div id="failure">
				<ul>
				<?php
					foreach($Err as $Er)
						print "<li>".$Er."</li>";
				?>
				</ul>
			</div>
		  <?php } ?>
		  <?php if(array_count_values($Info)) { ?>
		  	<div id="info">
				<ul>
					<?php
						foreach($Info as $inf)
							print "<li>".$inf."</li>";
					?>
				</ul>
			</div>
		  <?php } ?>
		  </div><div>&nbsp;</div>
		  <?php if($HTTP_SESSION_VARS['Stage']==1) { ?>
		  <fieldset>
            <legend>Security Lock</legend>
            
            <form action="<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>" method="post">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <!--DWLayoutTable-->
                <tr>
                  <td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['SecurityCode']; ?>">
                    <!--DWLayoutTable-->
                      <tr>
                        <td width="20" rowspan="2" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                        <td colspan="2" valign="top"><label>Security Code</label></td>
                        <td width="316" valign="top"><input name="SecurityCode" type="text" id="SecurityCode" value="<?php print $HTTP_POST_VARS['SecurityCode']; ?>" size="50"></td>
                        <td width="100%" valign="top"><span class="Help">TIP: This is the same Security Code that was required when you installed HT Installer</span></td>
                      </tr>
                      <tr>
                        <td width="100"></td>
                        <td width="10"></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="spacer.gif" alt="" width="20" height="1"></td>
                        <td><img src="spacer.gif" alt="" width="100" height="1"></td>
                        <td colspan="2"><img src="spacer.gif" alt="" width="326" height="1"></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="spacer.gif" alt="" width="20" height="1"></td>
                        <td><img src="spacer.gif" alt="" width="100" height="1"></td>
                        <td><img src="spacer.gif" alt="" width="10" height="1"></td>
                        <td><img src="spacer.gif" alt="" width="316" height="1"></td>
                        <td></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <!--DWLayoutTable-->
                <tr>
                  <td width="21" height="28" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                  <td width="290" valign="top"><input name="Action" type="submit" class="action" id="Action" value="Unlock"></td>
                  <td width="100%" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                  <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                </tr>
                <tr>
                  <td height="0"></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td height="1"><img src="spacer.gif" alt="" width="21" height="1"></td>
                  <td><img src="spacer.gif" alt="" width="290" height="1"></td>
                  <td></td>
                  <td><img src="spacer.gif" alt="" width="5" height="1"></td>
                </tr>
              </table>
            </form>
            </fieldset>
			<?php } ?>
		  <?php if($HTTP_SESSION_VARS['Stage']==2) { ?>
		  <fieldset>
            <legend>Admin Account</legend>
            <form action="<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>" method="post">
			<table width="100%" border="0" cellpadding="0" cellspacing="0"><!--DWLayoutTable-->
				<tr>
					<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['UserName']; ?>">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="142" valign="top"><label for="UserName">UserName</label></td>
							  <td width="304" valign="top"><input name="UserName" type="text" id="UserName" value="<?php print $HTTP_POST_VARS['UserName']; ?>" size="50"></td>
							    <td width="100%" valign="top"><span class="Help">TIP: Enter a UserName. This field may contain letters and numbers only. </span></td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="142" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['Password']; ?>">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%" rowspan="2"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="143" rowspan="2" valign="top"><label for="Password">Password</label></td>
							    <td width="304" height="26" valign="top"><input name="Password" type="password" id="Password" size="50"></td>
							    <td width="100%" rowspan="2" valign="top"><span class="Help">TIP: Enter a password</span></td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="143" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['RePassword']; ?>">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="142" valign="top"><label for="RePassword">Re-Type Password</label></td>
							  <td width="304" valign="top"><input name="RePassword" type="password" id="RePassword" size="50"></td>
							    <td width="100%" valign="top"><span class="Help">TIP: Enter a password</span></td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="142" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="142" valign="top"><label for="RePassword">
								<input name="Action" type="submit" class="action" id="Action" value="Create">
								</label></td>
								<td width="304" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="100%" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="142" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>					</td>
				</tr>
			</table>
            </form>
            </fieldset>
			<?php } ?>
		  <?php if($HTTP_SESSION_VARS['Stage']==3) { ?>
		  <fieldset>
            <legend>Login to HT Installer</legend>
            <form action="<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>" method="post">
			<table width="100%" border="0" cellpadding="0" cellspacing="0"><!--DWLayoutTable-->
				<tr>
					<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['UserName']; ?>">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="143" valign="top"><label for="UserName">UserName</label></td>
							  <td width="304" valign="top"><input name="UserName" type="text" id="UserName" value="<?php print $HTTP_POST_VARS['UserName']; ?>" size="50"></td>
							    <td width="100%" valign="top"><span class="Help">TIP: Enter your UserName </span></td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="142" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="<?php print $State['Password']; ?>">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="143" valign="top"><label for="Password">Password</label></td>
							  <td width="304" valign="top"><input name="Password" type="password" id="Password" size="50"></td>
							    <td width="100%" valign="top"><span class="Help">TIP: Enter your password</span></td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="143" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="143" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
						  </tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
						  <!--DWLayoutTable-->
							<tr>
								<td width="3%"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="142" valign="top"><label for="RePassword">
								<input name="Action" type="submit" class="action" id="Action" value="Login">
								</label></td>
								<td width="304" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
								<td width="100%" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
							</tr>
							<tr>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
					      </tr>
							<tr>
							  <td><img src="spacer.gif" alt="" width="26" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="142" height="1"></td>
							  <td><img src="spacer.gif" alt="" width="304" height="1"></td>
							  <td></td>
					      </tr>
						</table>					</td>
				</tr>
			</table>
            </form>
            </fieldset>
			<?php } ?>
			<?php if($HTTP_SESSION_VARS['Stage']==4) { ?>
				<table width="100%" cellpadding="0" cellspacing="0" border="0"><!--DWLayoutTable-->
					<tr>
						<td width="100%">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<div id="MenuBlock">
											<!-- tabs -->
										</div>
										<div id="TabDescription"><span id="DescData"><!-- tab descriptions --></span>
										<span id="Status"><a href="#" onClick="javascript: UpdatePackageList(); return false;">[ Refresh ]</a> <a href="#" onClick="javascript: document.getElementById('msg').innerHTML=''; return false;">[Clear Messages]</a></span></div>									</td>
								</tr>
							</table>
							<table width="100%" border="0" cellpadding="0" cellspacing="0"><!--DWLayoutTable-->
								<tr>
									<td width="100%">
										<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="1" height="1" title="uploadSWF">
											<param name="movie" value="upload.swf">
											<param name="quality" value="high">
											<embed hidden="true" id="uploadSWF" src="upload.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="1" height="1"></embed>
										</object>
										<div class="TabPage" id="InstallPackageID">
											<form>
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td>
														<label id="INHouse" for="InstallPackageList">Package</label>&nbsp;													</td>
													<td>:
														<select id="InstallPackageList" onChange="javascript: document.getElementById('SubPath').value=''; return false;">
															<option value="">==| Select Package |==</option>
														</select>													</td>
												</tr>
												<tr>
													<td><label id="INHouse" for="SubPath">Path to extract from</label>&nbsp;</td>
													<td>:
														<input type="text" id="SubPath" size="130">&nbsp;
														<input type="button" class="action" value="Browse" onClick="javascript: ListContents(''); return false;">													</td>
												</tr>
												<tr>
													<td><label id="INHouse" for="ExtractPath">Extract Path</label>&nbsp;</td>
													<td>:
														<input type="text" id="ExtractPath" size="130" value="<?php print dirname($_SERVER['SCRIPT_FILENAME']); ?>/">													</td>
												</tr>
												<tr>
													<td><input type="button" id="info" value="Install" onClick="javascript: StartInstall(); return false;"></td>
													<td>&nbsp;</td>
												</tr>
											</table>
											<table cellpadding="0" cellspacing="0" border="0" id="_SubFileList">
											</table>
											</form>
										</div>
										<div class="TabPage" id="AboutHTInstallerID">
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
											  <!--DWLayoutTable-->
												<tr>
													<td width="160" height="15" valign="top">Product Name</td>
													<td width="100%" valign="top">: HT Installer</td>
												</tr>
												<tr>
													<td height="18" valign="top">Version</td>
													<td valign="top">: <?php print HTI_VERSION; ?></td>
												</tr>
												<tr>
													<td>Author</td>
													<td>: Mathew Paret</td>
												</tr>
												<tr>
													<td>Website</td>
													<td>: <a href='http://www.glibix.com'>http://www.glibix.com</a></td>
												</tr>
												<tr>
													<td>Product HomePage</td>
													<td>: <a href="http://www.glibix.com/?q=products/ht-installer">http://www.glibix.com/products/ht-installer</a>												</td>
												<tr>
													<td>Support Email</td>
													<td>: <a href="mailto:support@glibix.com">support@glibix.com</a></td>
												</tr>
												<tr>
													<td>Log File</td>
													<td>: 
													<span id='LogFileSize'>
														<?php
															if(file_exists(DPX_HT_INSTALLER_PATH."/log.txt"))
																print perfectsize(filesize(DPX_HT_INSTALLER_PATH."/log.txt"));
															else
																print "Not Found!";
														?>
													</span>													</td>
												</tr>
												<tr>
													<td>Last Installed On</td>
													<td>: <?php print date("F j, Y, g:i a",filectime(DPX_HT_INSTALLER_PATH."/config.php")); ?></td>
												</tr>
												<tr>
													<td>Server</td>
													<td>: <?php print $_SERVER['SERVER_NAME']; ?></td>
												</tr>
												<tr>
													<td>Install Path</td>
													<td>: <?php print dirname($_SERVER['SCRIPT_FILENAME']);?></td>
												</tr>
												<tr>
													<td>Backup Packages</td>
													<td>: <span id='TotalPackageSize'><?php print perfectsize(DirSize(DPX_HT_INSTALLER_PATH."/packages/")); ?></span></td>
												</tr>
												<tr>
												  <td height="0"></td>
												  <td></td>
											  </tr>
												<tr>
												  <td height="1"><img src="spacer.gif" alt="" width="160" height="1"></td>
												  <td></td>
											  </tr>
											</table>
										</div>
										<div class="TabPage" id="SettingsID">
											<form>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><label id="INHouse" for="ChunkSize">Chunk Size</label></td>
														<td>: <input type="text" name="ChunkSize" id="ChunkSize" value="<?php print $Config['ChunkSize']; ?>"> <label id="INHouse" for="ChunkSize">bytes</label></td>
														<td><span class="Help"><font color="#FFFFFF">Increase or Decrease this value to set the amount of bytes to download in one go to avoid server timeout.</font></span></td>
													</tr>
													<tr>
														<td><label id="INHouse" for="Debug">Debug</label></td>
														<td>:<input type="checkbox" id="Debug" name="Debug" value="<?php print $Config['Debug']; ?>" <?php if($Config['Debug']) { ?> checked="checked" <?php } ?>></td>
														<td><span class="Help"><font color="#FFFFFF">Enable DEBUG mode?</font></span></td>
													</tr>
													<tr>
														<td><label id="INHouse" for="PageSize">Page Size</label></td>
														<td>: <input type="text" name="PageSize" id="PageSize" value="<?php print $Config['PageSize']; ?>"></td>
														<td><span class="Help"><font color="#FFFFFF">No of log entries to show in one page.</font></span></td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td>
															<center>
																<input type="button" value="Save Settings" id="info" onClick="javascript: SaveSettings(); return false;">
															</center>														</td>
													</tr>
												</table>
											</form>
										</div>
										<div class="TabPage" id="StatusPageID">
											<ol id="StatusDisplayDiv">
											</ol>
										</div>
										<div class="TabPage" id="LogID">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" id="LogHeaderID">
											  <!--DWLayoutTable-->
												<tr>
													<td width="147" height="18" valign="top"><strong>Date / Time</strong></td>
													<td width="52" valign="top"><strong>Type</strong></td>
													<td width="100%" valign="top"><strong>Message</strong></td>
													<td width="113" valign="top"><strong>Details</strong></td>
												</tr>
												<tr>
												  <td height="0"></td>
												  <td></td>
												  <td></td>
												  <td></td>
											  </tr>
												<tr>
												  <td height="1"><img src="spacer.gif" alt="" width="147" height="1"></td>
												  <td><img src="spacer.gif" alt="" width="52" height="1"></td>
												  <td></td>
												  <td><img src="spacer.gif" alt="" width="113" height="1"></td>
											  </tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0" id="LogDisplayID">
											</table>
										</div>
										<div class="TabPage" id="AccountSettingsID">
											<form>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><label id="INHouse" for="UserName">UserName</label></td>
														<td>: <input id="UserName" name="UserName" type="text" value="<?php print $Config['UserName']; ?>"></td>
														<td><Span class="Help"><font color="#FFFFFF">Change your username here</font></Span></td>
													</tr>
													<tr>
														<td><label id="INHouse" for="OldPassword">Current Password</label></td>
														<td>: <input type="password" name="OldPassword" id="OldPassword"></td>
														<td><span class="Help"><font color="#FFFFFF">Enter your current password here</font></span></td>
													</tr>
													<tr>
														<td><label id="INHouse" for="NewPassword">New Password</label></td>
														<td>: <input  type="password" name="NewPassword" id="NewPassword"></td>
														<td><span class="Help"><font color="#FFFFFF">Enter your new password here</font></span></td>
													</tr>
													<tr>
														<td><label id="INHouse" for="RePassword">New Password</label></td>
														<td>: <input  type="password" name="RePassword" id="RePassword"></td>
														<td><span class="Help"><font color="#FFFFFF">Re-Enter your new password here</font></span></td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td>
															<center>
																<input id="info" type="button" value="Save" onClick="javascript: SaveUser(); return false;">
															</center>														</td>
													</tr>
												</table>
											</form>
										</div>
										<div class="TabPage" id="BackupPackagesID">
											<form action="<?php print $HTTP_SERVER_VARS['PHP_SELF']; ?>" method="post">
												<table width="100%" border="0" cellpadding="0" cellspacing="0" id="List">
												  <!--DWLayoutTable-->
													<tr id="ListHeader">
														<td width="20" height="20" valign="top"><input name="CheckAll" type="checkbox" id="CheckAll" onClick="javascript: CheckAllClick();"></td>
														<td width="100%" valign="top"><strong>Package Name</strong></td>
														<td width="74" valign="top"><div align="left"><strong>Size</strong></div></td>
														<td width="244" valign="top"><div align="center"><strong>Date</strong></div></td>
														<td width="172" valign="top"><div align="center"><strong>Operations</strong></div></td>
													</tr>
													<tr>
													  <td height="0"></td>
													  <td></td>
													  <td></td>
													  <td></td>
													  <td></td>
												  </tr>
													<tr>
													  <td height="1"><img src="spacer.gif" alt="" width="20" height="1"></td>
													  <td></td>
													  <td><img src="spacer.gif" alt="" width="74" height="1"></td>
													  <td><img src="spacer.gif" alt="" width="244" height="1"></td>
													  <td><img src="spacer.gif" alt="" width="172" height="1"></td>
												  </tr>
												</table>										
												<table width="100%" border="0" cellpadding="0" cellspacing="0" id="divPackageList">
												</table>
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td width='100%'>
															<center>You may <a href='#' onClick='javascript: UploadPackage(); return false;'>Upload</a> or <a href='#' onClick='javascript: DownloadToServer(); return false;'>Download</a> a new package.</center>														</td>
													</tr>
													<tr>
														<td><img src='spacer.gif' width='100%' height='1'></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><input type="button" name="Action" value="Delete" onClick="javascript: DeleteCheckedPackages(); return false;" id="error"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0" id="" style="">
													<tr>
														<table width="100%" border="0" cellpadding="0" cellspacing="0" id="DownloadServerID" style="display:none">
															<tr>
																<td><label for="DownloadURL">URL &nbsp;</label></td>
																<td width="100%">&nbsp;<input type="text" name="DownloadURL" id="DownloadURL" placeholder="http://www.example.com/path/to/filename.zip"></td>
																<td>&nbsp;<input type="button" name="Download" value="Download" onClick="javascript: AjaxStartDownload(); return false;" id="info"></td>
																<td>&nbsp;<input type="button" name="Cancel" value="Cancel" onClick="javascript: document.getElementById('DownloadServerID').style.display='none'; return false;" id="info"></td>
															</tr>
														</table>
													</tr>
												</table>
											</form>
										</div>									</td>
								</tr>
						  </table>						</td>
					</tr>
				</table>
			<?php } ?>		  </td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
          <td width="100%" valign="top" bgcolor="#000000"><div align="center" class="footer">&copy; DeProxy Solution 2007
              <?php if(date('Y')>2007) print " - ".date('Y'); ?>
              | Designed By: Mathew Paret | <a href="http://glibix.com/products/ht-installer">HT Installer v<?php print HTI_VERSION; ?></a> | <a href="http://www.glibix.com">http://www.glibix.com</a> </div></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
